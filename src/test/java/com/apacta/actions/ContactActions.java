package com.apacta.actions;

import com.apacta.models.Contact;
import com.apacta.models.ContactDto;
import com.apacta.models.TimeEntryDto;
import com.apacta.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.lang.reflect.Type;
import java.util.UUID;

public class ContactActions {
    public Response response;
    private static final String API_ENDPOINT = "?api_key=";
    private static final String CONTACT_MANAGEMENT_ENDPOINT = "contacts";

    @Step("Retrieve contact")
    public void requestContactWithGetMethod() throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s%s%s", Constants.BASE_URL, CONTACT_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve response body for Get contact")
    public Contact getBody() throws Exception {
        Contact contact = response.then().extract().as((Type) Contact.class);
        return contact;
    }

    @Step("Retrieve Url with post method")
    public void requestSentForCreateContactWithPostMethod(String address, String city_id, String cvr, String description, String email, String erp_id) throws Exception {
        ContactDto contactDto = new ContactDto();
        contactDto.setAddress(address);
        contactDto.setCityId(UUID.fromString(city_id));
        contactDto.setCvr(cvr);
        contactDto.setDescription(description);
        contactDto.setEmail(email);
        contactDto.setErp_id(erp_id);

        response = SerenityRest.given().body(contactDto.toJSONString())
                .header(("content-type"), "application/json")
                .post(String.format("%s/%s%s%s", Constants.BASE_URL, CONTACT_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }
}
