package com.apacta.actions;

import com.apacta.models.TimeEntry;
import com.apacta.models.TimeEntryDto;
import com.apacta.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.lang.reflect.Type;
import java.util.UUID;

public class TimeEntriesActions {
    public Response response;
    private static final String API_ENDPOINT = "?api_key=";
    private static final String TIME_ENTRIES_MANAGEMENT_ENDPOINT = "time_entries";

    @Step("Retrieve time entries")
    public void requestTimeEntriesWithGetMethod() throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s%s%s", Constants.BASE_URL, TIME_ENTRIES_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve response body for Get time entries")
    public TimeEntry getBody() throws Exception {
        TimeEntry timeEntry = response.then().extract().as((Type) TimeEntry.class);
        return timeEntry;
    }

    @Step("Retrieve Url with post method")
    public void requestSentForCreateInvoiceWithPostMethod(String form_id, String from_time, String is_all_day, String project_id, String sum, String time_entry_type_id, String to_time, String user_id) throws Exception {
        TimeEntryDto timeEntryDto = new TimeEntryDto();
        timeEntryDto.setForm_id(UUID.fromString(form_id));
        timeEntryDto.setFrom_time(from_time);
        timeEntryDto.setIs_all_day(Boolean.valueOf(is_all_day));
        timeEntryDto.setProject_id(project_id);
        timeEntryDto.setSum(Integer.valueOf(sum));
        timeEntryDto.setTime_entry_type_id(time_entry_type_id);
        timeEntryDto.setTo_time(to_time);
        timeEntryDto.setUser_id(UUID.fromString(user_id));

        response = SerenityRest.given().body(timeEntryDto.toJSONString())
                .header(("content-type"), "application/json")
                .post(String.format("%s/%s%s%s", Constants.BASE_URL, TIME_ENTRIES_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }
}
