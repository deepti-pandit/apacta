package com.apacta.actions;

import com.apacta.models.City;
import com.apacta.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.lang.reflect.Type;

public class CityActions {
    public Response response;
    private static final String API_ENDPOINT = "?api_key=";
    private static final String CITY_MANAGEMENT_ENDPOINT = "cities";

    @Step("Retrieve city")
    public void requestCityWithGetMethod() throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s%s%s", Constants.BASE_URL, CITY_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve response body for Get city")
    public City getBody() throws Exception {
        City city = response.then().extract().as((Type) City.class);
        return city;
    }
}
