/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import java.util.UUID;

/**
 * TimeEntry
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class TimeEntryDto {

    public static final String SERIALIZED_NAME_CREATED = "created";
    public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
    public static final String SERIALIZED_NAME_DELETED = "deleted";
    public static final String SERIALIZED_NAME_FORM_ID = "form_id";
    public static final String SERIALIZED_NAME_FROM_TIME = "from_time";
    public static final String SERIALIZED_NAME_ID = "id";
    public static final String SERIALIZED_NAME_IS_ALL_DAY = "is_all_day";
    public static final String SERIALIZED_NAME_MODIFIED = "modified";
    public static final String SERIALIZED_NAME_MODIFIED_BY_ID = "modified_by_id";
    public static final String SERIALIZED_NAME_PROJECT_ID = "project_id";
    public static final String SERIALIZED_NAME_SUM = "sum";
    public static final String SERIALIZED_NAME_TIME_ENTRY_TYPE_ID = "time_entry_type_id";
    public static final String SERIALIZED_NAME_TO_TIME = "to_time";
    public static final String SERIALIZED_NAME_USER_ID = "user_id";
    /**
     * READ-ONLY
     *
     * @return project
     **/

    public static final String SERIALIZED_NAME_PROJECT = "project";
    /**
     * READ-ONLY
     *
     * @return user
     **/

    public static final String SERIALIZED_NAME_USER = "user";
    public String contact_id;
    public String date;
    public String sender_id;
    public String is_calculated;
    public String time_entry_type;
    public String created_by_id;
    public String modified_by_id;
    public String payment_term_id;
    public String company_id;
    public String integration_id;
    public String invoice_number;
    public String offer_number;
    public String title;
    public String issued_date;
    public String payment_due_date;
    public String date_from;
    public String date_to;
    public String is_draft;
    public String is_offer;
    public String is_locked;
    public String is_final_invoice;
    public String vat_percent;
    public String discount_percent;
    public String group_by_forms;
    public String erp_id;
    public String erp_payment_term_id;
    public String project_overview_attached;
    public String eu_customer;
    public String downloaded;
    public String include_invoiced_forms;
    public String combine_product_lines;
    public String combine_working_time_lines;
    public String show_payment_term;
    public String show_prices;
    public String show_product_images;
    public String show_employee_name;
    public String show_products_product_bundle;
    public String pdf_url;
    public String gross_payment;
    public String net_payment;
    public String total_discount_percent;
    public String has_project_pdf_attached;
    public String total_cost_price;
    @SerializedName(SERIALIZED_NAME_CREATED)
    private String created;
    @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
    private UUID createdById;
    @SerializedName(SERIALIZED_NAME_DELETED)
    private String deleted;
    @SerializedName(SERIALIZED_NAME_FORM_ID)
    private UUID form_id;
    @SerializedName(SERIALIZED_NAME_FROM_TIME)
    private String from_time;
    @SerializedName(SERIALIZED_NAME_ID)
    private UUID id;
    @SerializedName(SERIALIZED_NAME_IS_ALL_DAY)
    private Boolean is_all_day;
    @SerializedName(SERIALIZED_NAME_MODIFIED)
    private String modified;
    @SerializedName(SERIALIZED_NAME_MODIFIED_BY_ID)
    private UUID modifiedById;
    @SerializedName(SERIALIZED_NAME_PROJECT_ID)
    private String project_id;
    @SerializedName(SERIALIZED_NAME_SUM)
    private Integer sum;
    @SerializedName(SERIALIZED_NAME_TIME_ENTRY_TYPE_ID)
    private String time_entry_type_id;
    @SerializedName(SERIALIZED_NAME_TO_TIME)
    private String to_time;
    @SerializedName(SERIALIZED_NAME_USER_ID)
    private UUID user_id;
    @SerializedName(SERIALIZED_NAME_PROJECT)
    private Project project = null;
    @SerializedName(SERIALIZED_NAME_USER)
    private User user = null;

    public TimeEntryDto project(Project project) {
        this.project = project;
        return this;
    }

    public TimeEntryDto addProject(Project project) {
        this.project = project;
        return this;
    }

    @javax.annotation.Nullable
    @ApiModelProperty(value = "")
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public TimeEntryDto user(User user) {
        this.user = user;
        return this;
    }

    public TimeEntryDto addUser(User user) {
        this.user = user;
        return this;
    }

    @javax.annotation.Nullable
    @ApiModelProperty(value = "")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TimeEntryDto created(String created) {

        this.created = created;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return created
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public TimeEntryDto createdById(UUID createdById) {

        this.createdById = createdById;
        return this;
    }

    /**
     * Get createdById
     *
     * @return createdById
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getCreatedById() {
        return createdById;
    }


    public void setCreatedById(UUID createdById) {
        this.createdById = createdById;
    }


    public TimeEntryDto deleted(String deleted) {

        this.deleted = deleted;
        return this;
    }

    /**
     * READ-ONLY - only present if it&#39;s an deleted object
     *
     * @return deleted
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

    public String getDeleted() {
        return deleted;
    }


    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }


    public TimeEntryDto form_id(UUID form_id) {

        this.form_id = form_id;
        return this;
    }

    /**
     * Get form_id
     *
     * @return form_id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getForm_id() {
        return form_id;
    }


    public void setForm_id(UUID form_id) {
        this.form_id = form_id;
    }


    public TimeEntryDto from_time(String from_time) {

        this.from_time = from_time;
        return this;
    }

    /**
     * Get from_time
     *
     * @return from_time
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getFrom_time() {
        return from_time;
    }


    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }


    public TimeEntryDto id(UUID id) {

        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getId() {
        return id;
    }


    public void setId(UUID id) {
        this.id = id;
    }


    public TimeEntryDto is_all_day(Boolean is_all_day) {

        this.is_all_day = is_all_day;
        return this;
    }

    /**
     * Get is_all_day
     *
     * @return is_all_day
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getIs_all_day() {
        return is_all_day;
    }


    public void setIs_all_day(Boolean is_all_day) {
        this.is_all_day = is_all_day;
    }


    public TimeEntryDto modified(String modified) {

        this.modified = modified;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return modified
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getModified() {
        return modified;
    }


    public void setModified(String modified) {
        this.modified = modified;
    }


    public TimeEntryDto modifiedById(UUID modifiedById) {

        this.modifiedById = modifiedById;
        return this;
    }

    /**
     * Get modifiedById
     *
     * @return modifiedById
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getModifiedById() {
        return modifiedById;
    }


    public void setModifiedById(UUID modifiedById) {
        this.modifiedById = modifiedById;
    }


    public TimeEntryDto project_id(String project_id) {

        this.project_id = project_id;
        return this;
    }


    /**
     * Get project_id
     *
     * @return project_id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }


    public TimeEntryDto sum(Integer sum) {

        this.sum = sum;
        return this;
    }

    /**
     * Amount of seconds
     *
     * @return sum
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Amount of seconds")

    public Integer getSum() {
        return sum;
    }


    public void setSum(Integer sum) {
        this.sum = sum;
    }


    public TimeEntryDto timeEntryType_id(String time_entry_type_id) {

        this.time_entry_type_id = time_entry_type_id;
        return this;
    }

    /**
     * Get timeEntryTypeId
     *
     * @return timeEntryTypeId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getTime_entry_type_id() {
        return time_entry_type_id;
    }


    public void setTime_entry_type_id(String time_entry_type_id) {
        this.time_entry_type_id = time_entry_type_id;
    }


    public TimeEntryDto to_time(String to_time) {

        this.to_time = to_time;
        return this;
    }

    /**
     * Get toTime
     *
     * @return toTime
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getTo_time() {
        return to_time;
    }


    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }


    public TimeEntryDto user_id(UUID user_id) {

        this.user_id = user_id;
        return this;
    }

    /**
     * Get userId
     *
     * @return userId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getUser_id() {
        return user_id;
    }

    public void setUser_id(UUID user_id) {
        this.user_id = user_id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TimeEntryDto timeEntry = (TimeEntryDto) o;
        return Objects.equals(this.created, timeEntry.created) &&
                Objects.equals(this.createdById, timeEntry.createdById) &&
                Objects.equals(this.deleted, timeEntry.deleted) &&
                Objects.equals(this.form_id, timeEntry.form_id) &&
                Objects.equals(this.from_time, timeEntry.from_time) &&
                Objects.equals(this.id, timeEntry.id) &&
                Objects.equals(this.is_all_day, timeEntry.is_all_day) &&
                Objects.equals(this.modified, timeEntry.modified) &&
                Objects.equals(this.modifiedById, timeEntry.modifiedById) &&
                Objects.equals(this.project_id, timeEntry.project_id) &&
                Objects.equals(this.sum, timeEntry.sum) &&
                Objects.equals(this.time_entry_type_id, timeEntry.time_entry_type_id) &&
                Objects.equals(this.to_time, timeEntry.to_time) &&
                Objects.equals(this.user_id, timeEntry.user_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(created, createdById, deleted, form_id, from_time, id, is_all_day, modified, modifiedById, project_id, sum, time_entry_type_id, to_time, user_id);
    }


    public String toJSONString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" {\n");
        sb.append(" \"form_id\": ").append("\"" + toIndentedString(form_id) + "\"").append(",\n");
        sb.append(" \"from_time\": ").append("\"" + toIndentedString(from_time) + "\"").append(",\n");
        sb.append(" \"is_all_day\": ").append("\"" + toIndentedString(is_all_day) + "\"").append(",\n");
        sb.append(" \"project_id\": ").append("\"" + toIndentedString(project_id) + "\"").append(",\n");
        sb.append(" \"sum\": ").append(toIndentedString(sum)).append(",\n");
        sb.append(" \"time_entry_type_id\": ").append("\"" + toIndentedString(time_entry_type_id) + "\"").append(",\n");
        sb.append(" \"to_time\": ").append("\"" + toIndentedString(to_time) + "\"").append(",\n");
        sb.append(" \"user_id\": ").append("\"" + toIndentedString(user_id) + "\"").append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }


}

