/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.UUID;

import org.threeten.bp.OffsetDateTime;

/**
 * Company
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class Company {
    public static final String SERIALIZED_NAME_CITY_ID = "city_id";
    public static final String SERIALIZED_NAME_CONTACT_PERSON_ID = "contact_person_id";
    public static final String SERIALIZED_NAME_CREATED = "created";
    public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
    public static final String SERIALIZED_NAME_CVR = "cvr";
    public static final String SERIALIZED_NAME_DELETED = "deleted";
    public static final String SERIALIZED_NAME_EXPIRED = "expired";
    public static final String SERIALIZED_NAME_FILE_ID = "file_id";
    public static final String SERIALIZED_NAME_ID = "id";
    public static final String SERIALIZED_NAME_INVOICE_EMAIL = "invoice_email";
    public static final String SERIALIZED_NAME_LANGUAGE_ID = "language_id";
    public static final String SERIALIZED_NAME_MODIFIED = "modified";
    public static final String SERIALIZED_NAME_NAME = "name";
    public static final String SERIALIZED_NAME_NEXT_INVOICE_NUMBER = "next_invoice_number";
    public static final String SERIALIZED_NAME_PHONE = "phone";
    public static final String SERIALIZED_NAME_PHONE_COUNTRYCODE = "phone_countrycode";
    public static final String SERIALIZED_NAME_RECEIVE_FORM_MAILS = "receive_form_mails";
    public static final String SERIALIZED_NAME_STREET_NAME = "street_name";
    public static final String SERIALIZED_NAME_VAT_PERCENT = "vat_percent";
    public static final String SERIALIZED_NAME_WEBSITE = "website";
    @SerializedName(SERIALIZED_NAME_CITY_ID)
    private UUID cityId;
    @SerializedName(SERIALIZED_NAME_CONTACT_PERSON_ID)
    private UUID contactPersonId;
    @SerializedName(SERIALIZED_NAME_CREATED)
    private String created;
    @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
    private UUID createdById;
    @SerializedName(SERIALIZED_NAME_CVR)
    private String cvr;
    @SerializedName(SERIALIZED_NAME_DELETED)
    private String deleted;
    @SerializedName(SERIALIZED_NAME_EXPIRED)
    private OffsetDateTime expired;
    @SerializedName(SERIALIZED_NAME_FILE_ID)
    private UUID fileId;
    @SerializedName(SERIALIZED_NAME_ID)
    private UUID id;
    @SerializedName(SERIALIZED_NAME_INVOICE_EMAIL)
    private String invoiceEmail;
    @SerializedName(SERIALIZED_NAME_LANGUAGE_ID)
    private UUID languageId;
    @SerializedName(SERIALIZED_NAME_MODIFIED)
    private String modified;
    @SerializedName(SERIALIZED_NAME_NAME)
    private String name;
    @SerializedName(SERIALIZED_NAME_NEXT_INVOICE_NUMBER)
    private Integer nextInvoiceNumber;
    @SerializedName(SERIALIZED_NAME_PHONE)
    private String phone;
    @SerializedName(SERIALIZED_NAME_PHONE_COUNTRYCODE)
    private String phoneCountrycode;
    @SerializedName(SERIALIZED_NAME_RECEIVE_FORM_MAILS)
    private String receiveFormMails;
    @SerializedName(SERIALIZED_NAME_STREET_NAME)
    private String streetName;
    @SerializedName(SERIALIZED_NAME_VAT_PERCENT)
    private Integer vatPercent;
    @SerializedName(SERIALIZED_NAME_WEBSITE)
    private String website;


    public Company cityId(UUID cityId) {

        this.cityId = cityId;
        return this;
    }

    /**
     * Get cityId
     *
     * @return cityId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getCityId() {
        return cityId;
    }


    public void setCityId(UUID cityId) {
        this.cityId = cityId;
    }


    public Company contactPersonId(UUID contactPersonId) {

        this.contactPersonId = contactPersonId;
        return this;
    }

    /**
     * Get contactPersonId
     *
     * @return contactPersonId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getContactPersonId() {
        return contactPersonId;
    }


    public void setContactPersonId(UUID contactPersonId) {
        this.contactPersonId = contactPersonId;
    }


    public Company created(String created) {

        this.created = created;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return created
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public Company createdById(UUID createdById) {

        this.createdById = createdById;
        return this;
    }

    /**
     * Read-only
     *
     * @return createdById
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Read-only")

    public UUID getCreatedById() {
        return createdById;
    }


    public void setCreatedById(UUID createdById) {
        this.createdById = createdById;
    }


    public Company cvr(String cvr) {

        this.cvr = cvr;
        return this;
    }

    /**
     * Get cvr
     *
     * @return cvr
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getCvr() {
        return cvr;
    }


    public void setCvr(String cvr) {
        this.cvr = cvr;
    }


    public Company deleted(String deleted) {

        this.deleted = deleted;
        return this;
    }

    /**
     * READ-ONLY - only present if it&#39;s an deleted object
     *
     * @return deleted
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

    public String getDeleted() {
        return deleted;
    }


    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }


    public Company expired(OffsetDateTime expired) {

        this.expired = expired;
        return this;
    }

    /**
     * Get expired
     *
     * @return expired
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public OffsetDateTime getExpired() {
        return expired;
    }


    public void setExpired(OffsetDateTime expired) {
        this.expired = expired;
    }


    public Company fileId(UUID fileId) {

        this.fileId = fileId;
        return this;
    }

    /**
     * Get fileId
     *
     * @return fileId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getFileId() {
        return fileId;
    }


    public void setFileId(UUID fileId) {
        this.fileId = fileId;
    }


    public Company id(UUID id) {

        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getId() {
        return id;
    }


    public void setId(UUID id) {
        this.id = id;
    }


    public Company invoiceEmail(String invoiceEmail) {

        this.invoiceEmail = invoiceEmail;
        return this;
    }

    /**
     * Get invoiceEmail
     *
     * @return invoiceEmail
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getInvoiceEmail() {
        return invoiceEmail;
    }


    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }


    public Company languageId(UUID languageId) {

        this.languageId = languageId;
        return this;
    }

    /**
     * Get languageId
     *
     * @return languageId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getLanguageId() {
        return languageId;
    }


    public void setLanguageId(UUID languageId) {
        this.languageId = languageId;
    }


    public Company modified(String modified) {

        this.modified = modified;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return modified
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getModified() {
        return modified;
    }


    public void setModified(String modified) {
        this.modified = modified;
    }


    public Company name(String name) {

        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Company nextInvoiceNumber(Integer nextInvoiceNumber) {

        this.nextInvoiceNumber = nextInvoiceNumber;
        return this;
    }

    /**
     * Get nextInvoiceNumber
     *
     * @return nextInvoiceNumber
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Integer getNextInvoiceNumber() {
        return nextInvoiceNumber;
    }


    public void setNextInvoiceNumber(Integer nextInvoiceNumber) {
        this.nextInvoiceNumber = nextInvoiceNumber;
    }


    public Company phone(String phone) {

        this.phone = phone;
        return this;
    }

    /**
     * Format like eg. &#x60;28680133&#x60; or &#x60;046158971404&#x60;
     *
     * @return phone
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Format like eg. `28680133` or `046158971404`")

    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Company phoneCountrycode(String phoneCountrycode) {

        this.phoneCountrycode = phoneCountrycode;
        return this;
    }

    /**
     * Format like eg. &#x60;45&#x60; or &#x60;49&#x60;
     *
     * @return phoneCountrycode
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Format like eg. `45` or `49`")

    public String getPhoneCountrycode() {
        return phoneCountrycode;
    }


    public void setPhoneCountrycode(String phoneCountrycode) {
        this.phoneCountrycode = phoneCountrycode;
    }


    public Company receiveFormMails(String receiveFormMails) {

        this.receiveFormMails = receiveFormMails;
        return this;
    }

    /**
     * Get receiveFormMails
     *
     * @return receiveFormMails
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getReceiveFormMails() {
        return receiveFormMails;
    }


    public void setReceiveFormMails(String receiveFormMails) {
        this.receiveFormMails = receiveFormMails;
    }


    public Company streetName(String streetName) {

        this.streetName = streetName;
        return this;
    }

    /**
     * Get streetName
     *
     * @return streetName
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getStreetName() {
        return streetName;
    }


    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }


    public Company vatPercent(Integer vatPercent) {

        this.vatPercent = vatPercent;
        return this;
    }

    /**
     * Get vatPercent
     *
     * @return vatPercent
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Integer getVatPercent() {
        return vatPercent;
    }


    public void setVatPercent(Integer vatPercent) {
        this.vatPercent = vatPercent;
    }


    public Company website(String website) {

        this.website = website;
        return this;
    }

    /**
     * Get website
     *
     * @return website
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getWebsite() {
        return website;
    }


    public void setWebsite(String website) {
        this.website = website;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Company company = (Company) o;
        return Objects.equals(this.cityId, company.cityId) &&
                Objects.equals(this.contactPersonId, company.contactPersonId) &&
                Objects.equals(this.created, company.created) &&
                Objects.equals(this.createdById, company.createdById) &&
                Objects.equals(this.cvr, company.cvr) &&
                Objects.equals(this.deleted, company.deleted) &&
                Objects.equals(this.expired, company.expired) &&
                Objects.equals(this.fileId, company.fileId) &&
                Objects.equals(this.id, company.id) &&
                Objects.equals(this.invoiceEmail, company.invoiceEmail) &&
                Objects.equals(this.languageId, company.languageId) &&
                Objects.equals(this.modified, company.modified) &&
                Objects.equals(this.name, company.name) &&
                Objects.equals(this.nextInvoiceNumber, company.nextInvoiceNumber) &&
                Objects.equals(this.phone, company.phone) &&
                Objects.equals(this.phoneCountrycode, company.phoneCountrycode) &&
                Objects.equals(this.receiveFormMails, company.receiveFormMails) &&
                Objects.equals(this.streetName, company.streetName) &&
                Objects.equals(this.vatPercent, company.vatPercent) &&
                Objects.equals(this.website, company.website);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityId, contactPersonId, created, createdById, cvr, deleted, expired, fileId, id, invoiceEmail, languageId, modified, name, nextInvoiceNumber, phone, phoneCountrycode, receiveFormMails, streetName, vatPercent, website);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Company {\n");
        sb.append("    cityId: ").append(toIndentedString(cityId)).append("\n");
        sb.append("    contactPersonId: ").append(toIndentedString(contactPersonId)).append("\n");
        sb.append("    created: ").append(toIndentedString(created)).append("\n");
        sb.append("    createdById: ").append(toIndentedString(createdById)).append("\n");
        sb.append("    cvr: ").append(toIndentedString(cvr)).append("\n");
        sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
        sb.append("    expired: ").append(toIndentedString(expired)).append("\n");
        sb.append("    fileId: ").append(toIndentedString(fileId)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    invoiceEmail: ").append(toIndentedString(invoiceEmail)).append("\n");
        sb.append("    languageId: ").append(toIndentedString(languageId)).append("\n");
        sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    nextInvoiceNumber: ").append(toIndentedString(nextInvoiceNumber)).append("\n");
        sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
        sb.append("    phoneCountrycode: ").append(toIndentedString(phoneCountrycode)).append("\n");
        sb.append("    receiveFormMails: ").append(toIndentedString(receiveFormMails)).append("\n");
        sb.append("    streetName: ").append(toIndentedString(streetName)).append("\n");
        sb.append("    vatPercent: ").append(toIndentedString(vatPercent)).append("\n");
        sb.append("    website: ").append(toIndentedString(website)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}

