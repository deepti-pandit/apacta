package com.apacta.steps;

import com.apacta.actions.ContactActions;
import com.apacta.models.Contact;
import com.apacta.models.ContactDto;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ContactSteps {

    @Steps
    ContactActions contactActions = new ContactActions();

    @When("the user send a GET request for contact")
    public void theUserSendAGETRequestForContact() throws Exception {
        contactActions.requestContactWithGetMethod();
    }

    @Then("contact request should have status code as {int}")
    public void contactGetRequestShouldHaveStatusCodeAs(int statusCode) throws Exception {
        assertEquals("Status Code is not as expected", statusCode, contactActions.getStatusCode());
    }

    @And("Verify the fields in GET contact response$")
    public void verifyTheFieldsInGETContactResponse() throws Exception {
        Contact contact = contactActions.getBody();
        assertNotNull(contact.success);
        assertNotNull(contact.getContacts());
        ContactDto contactDto = contact.getContacts().get(0);
        assertNotNull(contactDto.getId());
        assertNotNull(contactDto.getName());
    }

    @When("user send POST request with settings address (.*), city_id (.*), cvr (.*), description (.*), email (.*), erp_id (.*)$")
    public void userSendPOSTRequestWithSettings(String address, String city_id, String cvr, String description, String email, String erp_id) throws Exception {
        contactActions.requestSentForCreateContactWithPostMethod(address, city_id, cvr, description, email, erp_id);
    }
}
