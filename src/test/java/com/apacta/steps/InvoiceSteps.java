package com.apacta.steps;

import com.apacta.actions.InvoiceActions;
import com.apacta.models.Invoice;
import com.apacta.models.InvoiceDto;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InvoiceSteps {

    @Steps
    InvoiceActions invoiceActions = new InvoiceActions();

    @When("the user send a GET request for invoices")
    public void theUserSendAGETRequestForInvoices() throws Exception {
        invoiceActions.requestInvoiceWithGetMethod();
    }

    @Then("invoices request should have status code as {int}")
    public void invoicesGetRequestShouldHaveStatusCodeAs(int statusCode) throws Exception {
        assertEquals("Status Code is not as expected", statusCode, invoiceActions.getStatusCode());
    }

    @And("Verify the fields in GET invoices response$")
    public void verifyTheFieldsInGETInvoiceResponse() throws Exception {
        Invoice invoice = invoiceActions.getBody();
        assertNotNull(invoice.success);
        assertNotNull(invoice.getInvoices());
        InvoiceDto invoiceDto = invoice.getInvoices().get(0);
        assertNotNull(invoiceDto.getId());
        assertNotNull(invoiceDto.getProject_id());
    }

    @When("the user send a DELETE request for invoices")
    public void theUserSendADELETERequestForInvoices() throws Exception {
        invoiceActions.requestInvoiceWithDeleteMethod();
    }

    @When("the user send a GET request for invoices with unauthorized user")
    public void theUserSendAGETRequestForInvoicesWithUnauthorizedUser() throws Exception {
        invoiceActions.requestInvoiceWithGetMethodForUnauthorizedUser();
    }
}
