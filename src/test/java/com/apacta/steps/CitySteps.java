package com.apacta.steps;

import com.apacta.actions.CityActions;
import com.apacta.models.City;
import com.apacta.models.CityDto;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CitySteps {

    @Steps
    CityActions cityActions = new CityActions();

    @When("the user send a GET request for city")
    public void theUserSendAGETRequestForCity() throws Exception {
        cityActions.requestCityWithGetMethod();
    }

    @Then("city get request should have status code as {int}")
    public void cityGetRequestShouldHaveStatusCodeAs(int statusCode) throws Exception {
        assertEquals("Status Code is not as expected", statusCode, cityActions.getStatusCode());
    }

    @And("Verify the fields in GET city response$")
    public void verifyTheFieldsInGETContactResponse() throws Exception {
        City city = cityActions.getBody();
        assertNotNull(city.success);
        assertNotNull(city.getCities());
        CityDto cityDto = city.getCities().get(0);
        assertNotNull(cityDto.getId());
        assertNotNull(cityDto.getName());
    }
}
