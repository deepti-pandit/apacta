Feature: Invoice management

  @GetInvoiceGET-200
  Scenario: Get invoices
    When the user send a GET request for invoices
    Then invoices request should have status code as 200
    And Verify the fields in GET invoices response

  @DeleteInvoiceDELETE-200
  Scenario: Delete invoice
    When the user send a DELETE request for invoices
    Then invoices request should have status code as 200

  @GetInvoiceGET-401
  Scenario: Get invoices with unauthorized user
    When the user send a GET request for invoices with unauthorized user
    Then invoices request should have status code as 401
