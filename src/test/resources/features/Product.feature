Feature: Product management

  @GetProductGET-200
  Scenario: Get product
    When the user send a GET request for product
    Then product get request should have status code as 200
    And Verify the fields in GET product response