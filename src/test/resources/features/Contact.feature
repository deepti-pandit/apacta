Feature: Contact management

  @GetContactGET-200
  Scenario: Get Contact
    When the user send a GET request for contact
    Then contact request should have status code as 200
    And Verify the fields in GET contact response

  @CreateContactPost-200
  Scenario Outline: POST contact
    When user send POST request with settings address <address>, city_id <city_id>, cvr <cvr>, description <description>, email <email>, erp_id <erp_id>
    Then contact request should have status code as 200
    Examples:
      | address   | city_id                              | cvr | description  | email            | erp_id |
      | amsterdam | bb1c6cd4-74fa-4c01-9e41-102952511f81 | abc | ams contract | deepti@gmail.com | 2314   |
