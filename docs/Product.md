

# Product

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**barcode** | **String** |  |  [optional]
**buyingPrice** | **Double** |  |  [optional]
**companyId** | [**UUID**](UUID.md) |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**erpId** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]
**productNumber** | **String** |  |  [optional]
**sellingPrice** | **Double** |  |  [optional]



