

# Contact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** | Street address |  [optional]
**cityId** | [**UUID**](UUID.md) |  |  [optional]
**companyId** | [**UUID**](UUID.md) | Only filled out if this represents another company within Apacta (used for partners) |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) | Read-only |  [optional]
**cvr** | **String** |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**erpId** | **String** | If company has integration to an ERP system, and the contacts are synchronized, this will be the ERP-systems ID of this contact |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]
**phone** | **String** | Format like eg. &#x60;28680133&#x60; or &#x60;046158971404&#x60; |  [optional]
**website** | **String** |  |  [optional]



