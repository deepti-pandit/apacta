

# InlineResponse2007

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Company**](Company.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



