

# MaterialRental

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Float** |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**fromDate** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**isInvoiced** | **String** |  |  [optional]
**materialId** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**modifiedById** | [**UUID**](UUID.md) |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**quantity** | **Float** |  |  [optional]
**toDate** | **String** |  |  [optional]



