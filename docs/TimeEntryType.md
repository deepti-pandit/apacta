

# TimeEntryType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyId** | [**UUID**](UUID.md) |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**modifiedById** | [**UUID**](UUID.md) |  |  [optional]
**name** | **String** |  |  [optional]
**timeEntryIntervalId** | [**UUID**](UUID.md) |  |  [optional]
**timeEntryValueTypeId** | [**UUID**](UUID.md) |  |  [optional]



