

# InlineResponse20015

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Currency**](Currency.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



