

# FormField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **String** |  |  [optional]
**contentValue** | **String** |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) | Read-only |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**fileId** | [**UUID**](UUID.md) |  |  [optional]
**formFieldTypeId** | [**UUID**](UUID.md) |  |  [optional]
**formId** | [**UUID**](UUID.md) |  |  [optional]
**formTemplateFieldId** | [**UUID**](UUID.md) |  |  [optional]
**id** | [**UUID**](UUID.md) | Read-only |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**placement** | **Integer** |  |  [optional]



