

# ExpenseLine

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buyingPrice** | **Float** |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) | Read-only |  [optional]
**currencyId** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**expenseId** | [**UUID**](UUID.md) |  |  [optional]
**id** | [**UUID**](UUID.md) | Read-only |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**quantity** | **Integer** |  |  [optional]
**sellingPrice** | **Float** |  |  [optional]
**text** | **String** |  |  [optional]



