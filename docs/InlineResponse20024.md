

# InlineResponse20024

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;FormFieldType&gt;**](FormFieldType.md) |  |  [optional]
**pagination** | [**PaginationDetails**](PaginationDetails.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



