

# Company

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cityId** | [**UUID**](UUID.md) |  |  [optional]
**contactPersonId** | [**UUID**](UUID.md) |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) | Read-only |  [optional]
**cvr** | **String** |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**expired** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**fileId** | [**UUID**](UUID.md) |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**invoiceEmail** | **String** |  |  [optional]
**languageId** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]
**nextInvoiceNumber** | **Integer** |  |  [optional]
**phone** | **String** | Format like eg. &#x60;28680133&#x60; or &#x60;046158971404&#x60; |  [optional]
**phoneCountrycode** | **String** | Format like eg. &#x60;45&#x60; or &#x60;49&#x60; |  [optional]
**receiveFormMails** | **String** |  |  [optional]
**streetName** | **String** |  |  [optional]
**vatPercent** | **Integer** |  |  [optional]
**website** | **String** |  |  [optional]



