

# IntegrationFeatureSetting

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**defaultValue** | **String** |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**identifier** | **String** |  |  [optional]
**integrationFeatureId** | [**UUID**](UUID.md) |  |  [optional]
**isCustomSetting** | **Boolean** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]



