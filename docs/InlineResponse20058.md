

# InlineResponse20058

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**TimeEntry**](TimeEntry.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



