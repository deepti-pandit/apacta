

# InlineResponse2006

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;Company&gt;**](Company.md) |  |  [optional]
**pagination** | [**PaginationDetails**](PaginationDetails.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



