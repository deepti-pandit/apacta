

# InlineResponse20030

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Form**](Form.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



