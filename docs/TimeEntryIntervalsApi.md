# TimeEntryIntervalsApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**timeEntryIntervalsGet**](TimeEntryIntervalsApi.md#timeEntryIntervalsGet) | **GET** /time_entry_intervals | List possible time entry intervals
[**timeEntryIntervalsTimeEntryIntervalIdGet**](TimeEntryIntervalsApi.md#timeEntryIntervalsTimeEntryIntervalIdGet) | **GET** /time_entry_intervals/{time_entry_interval_id} | View time entry interval


<a name="timeEntryIntervalsGet"></a>
# **timeEntryIntervalsGet**
> InlineResponse20059 timeEntryIntervalsGet()

List possible time entry intervals

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.TimeEntryIntervalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    TimeEntryIntervalsApi apiInstance = new TimeEntryIntervalsApi(defaultClient);
    try {
      InlineResponse20059 result = apiInstance.timeEntryIntervalsGet();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TimeEntryIntervalsApi#timeEntryIntervalsGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20059**](InlineResponse20059.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="timeEntryIntervalsTimeEntryIntervalIdGet"></a>
# **timeEntryIntervalsTimeEntryIntervalIdGet**
> InlineResponse20060 timeEntryIntervalsTimeEntryIntervalIdGet(timeEntryIntervalId)

View time entry interval

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.TimeEntryIntervalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    TimeEntryIntervalsApi apiInstance = new TimeEntryIntervalsApi(defaultClient);
    UUID timeEntryIntervalId = new UUID(); // UUID | 
    try {
      InlineResponse20060 result = apiInstance.timeEntryIntervalsTimeEntryIntervalIdGet(timeEntryIntervalId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TimeEntryIntervalsApi#timeEntryIntervalsTimeEntryIntervalIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryIntervalId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20060**](InlineResponse20060.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

