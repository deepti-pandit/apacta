

# ExpenseFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) | Read-only |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**expenseId** | [**UUID**](UUID.md) |  |  [optional]
**file** | **String** | File&#39;s name |  [optional]
**fileExtension** | **String** |  |  [optional]
**fileOriginalName** | **String** |  |  [optional]
**fileSize** | **String** | File size in bytes |  [optional]
**fileType** | **String** | File&#39;s MIME type |  [optional]
**fileUrl** | **String** | Read-only |  [optional]
**id** | [**UUID**](UUID.md) | Read-only |  [optional]
**modified** | **String** | READ-ONLY |  [optional]



