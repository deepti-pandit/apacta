

# InlineResponse20029

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;Form&gt;**](Form.md) |  |  [optional]
**pagination** | [**PaginationDetails**](PaginationDetails.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



