

# InlineObject3

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buyingPrice** | **Float** |  |  [optional]
**currencyId** | [**UUID**](UUID.md) |  |  [optional]
**expenseId** | [**UUID**](UUID.md) |  |  [optional]
**quantity** | **Integer** |  |  [optional]
**sellingPrice** | **Float** |  |  [optional]
**text** | **String** |  |  [optional]



