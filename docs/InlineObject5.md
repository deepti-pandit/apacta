

# InlineObject5

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **String** |  |  [optional]
**contentValue** | **String** |  |  [optional]
**fileId** | [**UUID**](UUID.md) |  |  [optional]
**formFieldTypeId** | [**UUID**](UUID.md) |  |  [optional]
**formId** | [**UUID**](UUID.md) |  |  [optional]
**formTemplateFieldId** | [**UUID**](UUID.md) |  |  [optional]
**placement** | **Integer** |  |  [optional]



