

# InlineObject12

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**formId** | [**UUID**](UUID.md) |  |  [optional]
**materialRentalId** | [**UUID**](UUID.md) |  |  [optional]
**toDate** | **String** |  |  [optional]



