

# FormTemplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**formCategoryId** | [**UUID**](UUID.md) |  |  [optional]
**formOverviewCategoryId** | [**UUID**](UUID.md) |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**identifier** | **String** |  |  [optional]
**isActive** | **Boolean** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]
**pdfTemplateIdentifier** | **String** |  |  [optional]



