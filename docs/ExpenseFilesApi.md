# ExpenseFilesApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**expenseFilesExpenseFileIdDelete**](ExpenseFilesApi.md#expenseFilesExpenseFileIdDelete) | **DELETE** /expense_files/{expense_file_id} | Delete file
[**expenseFilesExpenseFileIdGet**](ExpenseFilesApi.md#expenseFilesExpenseFileIdGet) | **GET** /expense_files/{expense_file_id} | Show file
[**expenseFilesExpenseFileIdPut**](ExpenseFilesApi.md#expenseFilesExpenseFileIdPut) | **PUT** /expense_files/{expense_file_id} | Edit file
[**expenseFilesGet**](ExpenseFilesApi.md#expenseFilesGet) | **GET** /expense_files | Show list of expense files
[**expenseFilesPost**](ExpenseFilesApi.md#expenseFilesPost) | **POST** /expense_files | Add file to expense


<a name="expenseFilesExpenseFileIdDelete"></a>
# **expenseFilesExpenseFileIdDelete**
> InlineResponse20018 expenseFilesExpenseFileIdDelete(expenseFileId)

Delete file

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseFilesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseFilesApi apiInstance = new ExpenseFilesApi(defaultClient);
    String expenseFileId = "expenseFileId_example"; // String | 
    try {
      InlineResponse20018 result = apiInstance.expenseFilesExpenseFileIdDelete(expenseFileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseFilesApi#expenseFilesExpenseFileIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseFileId** | **String**|  |

### Return type

[**InlineResponse20018**](InlineResponse20018.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="expenseFilesExpenseFileIdGet"></a>
# **expenseFilesExpenseFileIdGet**
> InlineResponse20018 expenseFilesExpenseFileIdGet(expenseFileId)

Show file

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseFilesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseFilesApi apiInstance = new ExpenseFilesApi(defaultClient);
    String expenseFileId = "expenseFileId_example"; // String | 
    try {
      InlineResponse20018 result = apiInstance.expenseFilesExpenseFileIdGet(expenseFileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseFilesApi#expenseFilesExpenseFileIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseFileId** | **String**|  |

### Return type

[**InlineResponse20018**](InlineResponse20018.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**404** | Not found |  -  |

<a name="expenseFilesExpenseFileIdPut"></a>
# **expenseFilesExpenseFileIdPut**
> InlineResponse20019 expenseFilesExpenseFileIdPut(expenseFileId)

Edit file

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseFilesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseFilesApi apiInstance = new ExpenseFilesApi(defaultClient);
    UUID expenseFileId = new UUID(); // UUID | 
    try {
      InlineResponse20019 result = apiInstance.expenseFilesExpenseFileIdPut(expenseFileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseFilesApi#expenseFilesExpenseFileIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseFileId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="expenseFilesGet"></a>
# **expenseFilesGet**
> InlineResponse20017 expenseFilesGet(createdById, expenseId)

Show list of expense files

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseFilesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseFilesApi apiInstance = new ExpenseFilesApi(defaultClient);
    UUID createdById = new UUID(); // UUID | 
    UUID expenseId = new UUID(); // UUID | 
    try {
      InlineResponse20017 result = apiInstance.expenseFilesGet(createdById, expenseId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseFilesApi#expenseFilesGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createdById** | [**UUID**](.md)|  | [optional]
 **expenseId** | [**UUID**](.md)|  | [optional]

### Return type

[**InlineResponse20017**](InlineResponse20017.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="expenseFilesPost"></a>
# **expenseFilesPost**
> InlineResponse201 expenseFilesPost(file, description)

Add file to expense

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseFilesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseFilesApi apiInstance = new ExpenseFilesApi(defaultClient);
    File file = new File("/path/to/file"); // File | 
    String description = "description_example"; // String | 
    try {
      InlineResponse201 result = apiInstance.expenseFilesPost(file, description);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseFilesApi#expenseFilesPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**|  |
 **description** | **String**|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: multipart/forms-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successfully added file |  -  |
**422** | Validation error |  -  |

