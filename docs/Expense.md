

# Expense

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyId** | [**UUID**](UUID.md) | Read-only |  [optional]
**contactId** | [**UUID**](UUID.md) |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) | Read-only |  [optional]
**currencyId** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**deliveryDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**description** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) | Read-only |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**reference** | **String** |  |  [optional]
**shortText** | **String** |  |  [optional]
**supplierInvoiceNumber** | **String** |  |  [optional]



