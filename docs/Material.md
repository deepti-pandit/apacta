

# Material

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**barcode** | **String** |  |  [optional]
**billingCycle** | **String** |  |  [optional]
**companyId** | [**UUID**](UUID.md) |  |  [optional]
**costPrice** | **Float** |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**isSingleUsage** | **Boolean** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**modifiedById** | [**UUID**](UUID.md) |  |  [optional]
**name** | **String** |  |  [optional]
**sellingPrice** | **Float** |  |  [optional]



