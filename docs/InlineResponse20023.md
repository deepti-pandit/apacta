

# InlineResponse20023

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Expense**](Expense.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



