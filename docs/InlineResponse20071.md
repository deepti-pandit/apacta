

# InlineResponse20071

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**WallPost**](WallPost.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



