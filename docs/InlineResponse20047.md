

# InlineResponse20047

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Product**](Product.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



