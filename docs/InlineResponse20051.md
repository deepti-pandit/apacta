

# InlineResponse20051

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Project**](Project.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



