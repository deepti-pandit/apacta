

# InlineResponse20054

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**User**](User.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



