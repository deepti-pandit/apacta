

# InlineResponse20016Data

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**formDate** | [**LocalDate**](LocalDate.md) | Y-m-d formatted |  [optional]
**formId** | [**UUID**](UUID.md) |  |  [optional]
**projectName** | **String** |  |  [optional]
**totalHours** | **Integer** | The amount of hours in seconds |  [optional]
**workingDescription** | **String** | Trimmed at 50 characters |  [optional]
**workingDescriptionFull** | **String** | Full work description (if available) |  [optional]



