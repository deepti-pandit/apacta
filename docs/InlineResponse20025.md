

# InlineResponse20025

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**FormFieldType**](FormFieldType.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



