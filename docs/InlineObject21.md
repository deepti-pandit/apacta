

# InlineObject21

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cityId** | [**UUID**](UUID.md) |  |  [optional]
**costPrice** | **Float** | Cost of salaries |  [optional]
**email** | **String** |  |  [optional]
**extraPrice** | **Float** | Additional cost on this employee (pension, vacation etc.) |  [optional]
**firstName** | **String** |  | 
**isActive** | **Boolean** |  |  [optional]
**languageId** | [**UUID**](UUID.md) |  |  [optional]
**lastName** | **String** |  |  [optional]
**mobile** | **String** |  |  [optional]
**mobileCountrycode** | **String** |  |  [optional]
**password** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**phoneCountrycode** | **String** |  |  [optional]
**receiveFormMails** | **Boolean** | If &#x60;true&#x60; the employee will receive an email receipt of every forms submitted |  [optional]
**salePrice** | **Float** | The price this employee costs per hour when working |  [optional]
**streetName** | **String** |  |  [optional]



