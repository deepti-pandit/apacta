

# InlineResponse20026

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**FormField**](FormField.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



