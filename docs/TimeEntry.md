

# TimeEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**formId** | [**UUID**](UUID.md) |  |  [optional]
**fromTime** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**isAllDay** | **Boolean** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**modifiedById** | [**UUID**](UUID.md) |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**sum** | **Integer** | Amount of seconds |  [optional]
**timeEntryTypeId** | [**UUID**](UUID.md) |  |  [optional]
**toTime** | **String** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]



