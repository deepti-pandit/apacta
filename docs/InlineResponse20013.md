

# InlineResponse20013

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Contact**](Contact.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



