# ExpenseLinesApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**expenseLinesExpenseLineIdDelete**](ExpenseLinesApi.md#expenseLinesExpenseLineIdDelete) | **DELETE** /expense_lines/{expense_line_id} | Delete expense line
[**expenseLinesExpenseLineIdGet**](ExpenseLinesApi.md#expenseLinesExpenseLineIdGet) | **GET** /expense_lines/{expense_line_id} | Show expense line
[**expenseLinesExpenseLineIdPut**](ExpenseLinesApi.md#expenseLinesExpenseLineIdPut) | **PUT** /expense_lines/{expense_line_id} | Edit expense line
[**expenseLinesGet**](ExpenseLinesApi.md#expenseLinesGet) | **GET** /expense_lines | Show list of expense lines
[**expenseLinesPost**](ExpenseLinesApi.md#expenseLinesPost) | **POST** /expense_lines | Add line to expense


<a name="expenseLinesExpenseLineIdDelete"></a>
# **expenseLinesExpenseLineIdDelete**
> InlineResponse20021 expenseLinesExpenseLineIdDelete(expenseLineId)

Delete expense line

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseLinesApi apiInstance = new ExpenseLinesApi(defaultClient);
    String expenseLineId = "expenseLineId_example"; // String | 
    try {
      InlineResponse20021 result = apiInstance.expenseLinesExpenseLineIdDelete(expenseLineId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseLinesApi#expenseLinesExpenseLineIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseLineId** | **String**|  |

### Return type

[**InlineResponse20021**](InlineResponse20021.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="expenseLinesExpenseLineIdGet"></a>
# **expenseLinesExpenseLineIdGet**
> InlineResponse20021 expenseLinesExpenseLineIdGet(expenseLineId)

Show expense line

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseLinesApi apiInstance = new ExpenseLinesApi(defaultClient);
    String expenseLineId = "expenseLineId_example"; // String | 
    try {
      InlineResponse20021 result = apiInstance.expenseLinesExpenseLineIdGet(expenseLineId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseLinesApi#expenseLinesExpenseLineIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseLineId** | **String**|  |

### Return type

[**InlineResponse20021**](InlineResponse20021.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**404** | Not found |  -  |

<a name="expenseLinesExpenseLineIdPut"></a>
# **expenseLinesExpenseLineIdPut**
> InlineResponse20021 expenseLinesExpenseLineIdPut(expenseLineId)

Edit expense line

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseLinesApi apiInstance = new ExpenseLinesApi(defaultClient);
    String expenseLineId = "expenseLineId_example"; // String | 
    try {
      InlineResponse20021 result = apiInstance.expenseLinesExpenseLineIdPut(expenseLineId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseLinesApi#expenseLinesExpenseLineIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseLineId** | **String**|  |

### Return type

[**InlineResponse20021**](InlineResponse20021.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="expenseLinesGet"></a>
# **expenseLinesGet**
> InlineResponse20020 expenseLinesGet(createdById, currencyId, expenseId)

Show list of expense lines

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseLinesApi apiInstance = new ExpenseLinesApi(defaultClient);
    UUID createdById = new UUID(); // UUID | 
    UUID currencyId = new UUID(); // UUID | 
    UUID expenseId = new UUID(); // UUID | 
    try {
      InlineResponse20020 result = apiInstance.expenseLinesGet(createdById, currencyId, expenseId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseLinesApi#expenseLinesGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createdById** | [**UUID**](.md)|  | [optional]
 **currencyId** | [**UUID**](.md)|  | [optional]
 **expenseId** | [**UUID**](.md)|  | [optional]

### Return type

[**InlineResponse20020**](InlineResponse20020.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="expenseLinesPost"></a>
# **expenseLinesPost**
> InlineResponse201 expenseLinesPost(expenseLine)

Add line to expense

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ExpenseLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ExpenseLinesApi apiInstance = new ExpenseLinesApi(defaultClient);
    InlineObject3 expenseLine = new InlineObject3(); // InlineObject3 | 
    try {
      InlineResponse201 result = apiInstance.expenseLinesPost(expenseLine);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExpenseLinesApi#expenseLinesPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseLine** | [**InlineObject3**](InlineObject3.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

