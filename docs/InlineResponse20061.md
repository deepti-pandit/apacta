

# InlineResponse20061

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;TimeEntryType&gt;**](TimeEntryType.md) |  |  [optional]
**pagination** | [**PaginationDetails**](PaginationDetails.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



