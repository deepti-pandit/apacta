

# MassMessagesUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**isRead** | **Boolean** |  |  [optional]
**isSentEmail** | **Boolean** |  |  [optional]
**massMessage** | [**MassMessage**](MassMessage.md) |  |  [optional]
**massMessageId** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**modifiedById** | [**UUID**](UUID.md) |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]



