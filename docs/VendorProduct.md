

# VendorProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**barcode** | **String** |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]
**price** | **Double** |  |  [optional]
**productCategoryNumber** | **String** |  |  [optional]
**productNumber** | **String** |  |  [optional]
**vendorId** | [**UUID**](UUID.md) |  |  [optional]



