# ClockingRecordsApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clockingRecordsCheckoutPost**](ClockingRecordsApi.md#clockingRecordsCheckoutPost) | **POST** /clocking_records/checkout | Checkout active clocking record for authenticated user
[**clockingRecordsClockingRecordIdDelete**](ClockingRecordsApi.md#clockingRecordsClockingRecordIdDelete) | **DELETE** /clocking_records/{clocking_record_id} | Delete a clocking record
[**clockingRecordsClockingRecordIdGet**](ClockingRecordsApi.md#clockingRecordsClockingRecordIdGet) | **GET** /clocking_records/{clocking_record_id} | Details of 1 clocking_record
[**clockingRecordsClockingRecordIdPut**](ClockingRecordsApi.md#clockingRecordsClockingRecordIdPut) | **PUT** /clocking_records/{clocking_record_id} | Edit a clocking record
[**clockingRecordsGet**](ClockingRecordsApi.md#clockingRecordsGet) | **GET** /clocking_records | Get a list of clocking records
[**clockingRecordsPost**](ClockingRecordsApi.md#clockingRecordsPost) | **POST** /clocking_records | Create clocking record for authenticated user


<a name="clockingRecordsCheckoutPost"></a>
# **clockingRecordsCheckoutPost**
> InlineResponse2011 clockingRecordsCheckoutPost()

Checkout active clocking record for authenticated user

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ClockingRecordsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ClockingRecordsApi apiInstance = new ClockingRecordsApi(defaultClient);
    try {
      InlineResponse2011 result = apiInstance.clockingRecordsCheckoutPost();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClockingRecordsApi#clockingRecordsCheckoutPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2011**](InlineResponse2011.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successfully checked out |  -  |
**422** | Validation error |  -  |

<a name="clockingRecordsClockingRecordIdDelete"></a>
# **clockingRecordsClockingRecordIdDelete**
> InlineResponse2005 clockingRecordsClockingRecordIdDelete(clockingRecordId)

Delete a clocking record

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ClockingRecordsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ClockingRecordsApi apiInstance = new ClockingRecordsApi(defaultClient);
    String clockingRecordId = "clockingRecordId_example"; // String | 
    try {
      InlineResponse2005 result = apiInstance.clockingRecordsClockingRecordIdDelete(clockingRecordId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClockingRecordsApi#clockingRecordsClockingRecordIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clockingRecordId** | **String**|  |

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="clockingRecordsClockingRecordIdGet"></a>
# **clockingRecordsClockingRecordIdGet**
> InlineResponse2003 clockingRecordsClockingRecordIdGet(clockingRecordId)

Details of 1 clocking_record

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ClockingRecordsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ClockingRecordsApi apiInstance = new ClockingRecordsApi(defaultClient);
    String clockingRecordId = "clockingRecordId_example"; // String | 
    try {
      InlineResponse2003 result = apiInstance.clockingRecordsClockingRecordIdGet(clockingRecordId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClockingRecordsApi#clockingRecordsClockingRecordIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clockingRecordId** | **String**|  |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**404** | Clocking record not found |  -  |

<a name="clockingRecordsClockingRecordIdPut"></a>
# **clockingRecordsClockingRecordIdPut**
> InlineResponse2004 clockingRecordsClockingRecordIdPut(clockingRecordId)

Edit a clocking record

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ClockingRecordsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ClockingRecordsApi apiInstance = new ClockingRecordsApi(defaultClient);
    String clockingRecordId = "clockingRecordId_example"; // String | 
    try {
      InlineResponse2004 result = apiInstance.clockingRecordsClockingRecordIdPut(clockingRecordId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClockingRecordsApi#clockingRecordsClockingRecordIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clockingRecordId** | **String**|  |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="clockingRecordsGet"></a>
# **clockingRecordsGet**
> InlineResponse2002 clockingRecordsGet(active)

Get a list of clocking records

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ClockingRecordsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ClockingRecordsApi apiInstance = new ClockingRecordsApi(defaultClient);
    Boolean active = true; // Boolean | Used to search for active clocking records
    try {
      InlineResponse2002 result = apiInstance.clockingRecordsGet(active);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClockingRecordsApi#clockingRecordsGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **active** | **Boolean**| Used to search for active clocking records | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="clockingRecordsPost"></a>
# **clockingRecordsPost**
> InlineResponse201 clockingRecordsPost(clockingRecord)

Create clocking record for authenticated user

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ClockingRecordsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ClockingRecordsApi apiInstance = new ClockingRecordsApi(defaultClient);
    InlineObject clockingRecord = new InlineObject(); // InlineObject | 
    try {
      InlineResponse201 result = apiInstance.clockingRecordsPost(clockingRecord);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClockingRecordsApi#clockingRecordsPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clockingRecord** | [**InlineObject**](InlineObject.md)|  |

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successfully added clocking record |  -  |
**422** | Validation error |  -  |

