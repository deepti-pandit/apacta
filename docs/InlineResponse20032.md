

# InlineResponse20032

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**InvoiceLine**](InvoiceLine.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



