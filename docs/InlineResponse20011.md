

# InlineResponse20011

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**ContactType**](ContactType.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



