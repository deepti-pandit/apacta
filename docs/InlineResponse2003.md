

# InlineResponse2003

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**ClockingRecord**](ClockingRecord.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



