

# ClockingRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checkedIn** | **String** |  |  [optional]
**checkedOut** | **String** |  |  [optional]
**checkinLatitude** | **String** |  |  [optional]
**checkinLongitude** | **String** |  |  [optional]
**checkoutLatitude** | **String** |  |  [optional]
**checkoutLongitude** | **String** |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**modifiedById** | [**UUID**](UUID.md) |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]



