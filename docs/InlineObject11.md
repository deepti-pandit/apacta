

# InlineObject11

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**formId** | [**UUID**](UUID.md) |  |  [optional]
**fromDate** | **String** |  |  [optional]
**isInvoiced** | **String** |  |  [optional]
**materialId** | [**UUID**](UUID.md) |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**quantity** | **Float** |  |  [optional]
**toDate** | **String** |  |  [optional]



