

# Form

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**approvedById** | [**UUID**](UUID.md) |  |  [optional]
**companyId** | [**UUID**](UUID.md) |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) | Read-only |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**formDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**formTemplateId** | [**UUID**](UUID.md) |  |  [optional]
**id** | [**UUID**](UUID.md) | Read-only |  [optional]
**isDraft** | **Boolean** |  |  [optional]
**isInvoiced** | **Boolean** |  |  [optional]
**isShared** | **Boolean** |  |  [optional]
**massFormId** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]



