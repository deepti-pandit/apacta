

# InlineResponse20042

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PaymentTermType**](PaymentTermType.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



