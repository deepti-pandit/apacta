# PaymentTermTypesApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentTermTypesGet**](PaymentTermTypesApi.md#paymentTermTypesGet) | **GET** /payment_term_types | Get a list of payment term types
[**paymentTermTypesPaymentTermTypeIdGet**](PaymentTermTypesApi.md#paymentTermTypesPaymentTermTypeIdGet) | **GET** /payment_term_types/{payment_term_type_id} | Details of 1 payment term type


<a name="paymentTermTypesGet"></a>
# **paymentTermTypesGet**
> InlineResponse20041 paymentTermTypesGet()

Get a list of payment term types

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.PaymentTermTypesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    PaymentTermTypesApi apiInstance = new PaymentTermTypesApi(defaultClient);
    try {
      InlineResponse20041 result = apiInstance.paymentTermTypesGet();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PaymentTermTypesApi#paymentTermTypesGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20041**](InlineResponse20041.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="paymentTermTypesPaymentTermTypeIdGet"></a>
# **paymentTermTypesPaymentTermTypeIdGet**
> InlineResponse20042 paymentTermTypesPaymentTermTypeIdGet(paymentTermTypeId)

Details of 1 payment term type

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.PaymentTermTypesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    PaymentTermTypesApi apiInstance = new PaymentTermTypesApi(defaultClient);
    String paymentTermTypeId = "paymentTermTypeId_example"; // String | 
    try {
      InlineResponse20042 result = apiInstance.paymentTermTypesPaymentTermTypeIdGet(paymentTermTypeId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PaymentTermTypesApi#paymentTermTypesPaymentTermTypeIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentTermTypeId** | **String**|  |

### Return type

[**InlineResponse20042**](InlineResponse20042.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**404** | Not found |  -  |

