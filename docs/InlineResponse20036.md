

# InlineResponse20036

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**MassMessagesUser**](MassMessagesUser.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



