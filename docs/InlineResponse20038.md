

# InlineResponse20038

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Material**](Material.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



