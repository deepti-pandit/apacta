# InvoiceLinesApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**invoiceLinesGet**](InvoiceLinesApi.md#invoiceLinesGet) | **GET** /invoice_lines | View list of invoice lines
[**invoiceLinesInvoiceLineIdDelete**](InvoiceLinesApi.md#invoiceLinesInvoiceLineIdDelete) | **DELETE** /invoice_lines/{invoice_line_id} | Delete invoice line
[**invoiceLinesInvoiceLineIdGet**](InvoiceLinesApi.md#invoiceLinesInvoiceLineIdGet) | **GET** /invoice_lines/{invoice_line_id} | View invoice line
[**invoiceLinesInvoiceLineIdPut**](InvoiceLinesApi.md#invoiceLinesInvoiceLineIdPut) | **PUT** /invoice_lines/{invoice_line_id} | Edit invoice line
[**invoiceLinesPost**](InvoiceLinesApi.md#invoiceLinesPost) | **POST** /invoice_lines | Add invoice


<a name="invoiceLinesGet"></a>
# **invoiceLinesGet**
> InlineResponse20031 invoiceLinesGet(invoiceId, productId, userId, name, discountText)

View list of invoice lines

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.InvoiceLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    InvoiceLinesApi apiInstance = new InvoiceLinesApi(defaultClient);
    UUID invoiceId = new UUID(); // UUID | 
    UUID productId = new UUID(); // UUID | 
    UUID userId = new UUID(); // UUID | 
    String name = "name_example"; // String | 
    String discountText = "discountText_example"; // String | 
    try {
      InlineResponse20031 result = apiInstance.invoiceLinesGet(invoiceId, productId, userId, name, discountText);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling InvoiceLinesApi#invoiceLinesGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | [**UUID**](.md)|  | [optional]
 **productId** | [**UUID**](.md)|  | [optional]
 **userId** | [**UUID**](.md)|  | [optional]
 **name** | **String**|  | [optional]
 **discountText** | **String**|  | [optional]

### Return type

[**InlineResponse20031**](InlineResponse20031.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="invoiceLinesInvoiceLineIdDelete"></a>
# **invoiceLinesInvoiceLineIdDelete**
> InlineResponse2004 invoiceLinesInvoiceLineIdDelete(invoiceLineId)

Delete invoice line

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.InvoiceLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    InvoiceLinesApi apiInstance = new InvoiceLinesApi(defaultClient);
    UUID invoiceLineId = new UUID(); // UUID | 
    try {
      InlineResponse2004 result = apiInstance.invoiceLinesInvoiceLineIdDelete(invoiceLineId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling InvoiceLinesApi#invoiceLinesInvoiceLineIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceLineId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="invoiceLinesInvoiceLineIdGet"></a>
# **invoiceLinesInvoiceLineIdGet**
> InlineResponse20032 invoiceLinesInvoiceLineIdGet(invoiceLineId)

View invoice line

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.InvoiceLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    InvoiceLinesApi apiInstance = new InvoiceLinesApi(defaultClient);
    UUID invoiceLineId = new UUID(); // UUID | 
    try {
      InlineResponse20032 result = apiInstance.invoiceLinesInvoiceLineIdGet(invoiceLineId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling InvoiceLinesApi#invoiceLinesInvoiceLineIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceLineId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20032**](InlineResponse20032.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="invoiceLinesInvoiceLineIdPut"></a>
# **invoiceLinesInvoiceLineIdPut**
> InlineResponse2004 invoiceLinesInvoiceLineIdPut(invoiceLineId, invoiceLine)

Edit invoice line

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.InvoiceLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    InvoiceLinesApi apiInstance = new InvoiceLinesApi(defaultClient);
    UUID invoiceLineId = new UUID(); // UUID | 
    InlineObject8 invoiceLine = new InlineObject8(); // InlineObject8 | 
    try {
      InlineResponse2004 result = apiInstance.invoiceLinesInvoiceLineIdPut(invoiceLineId, invoiceLine);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling InvoiceLinesApi#invoiceLinesInvoiceLineIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceLineId** | [**UUID**](.md)|  |
 **invoiceLine** | [**InlineObject8**](InlineObject8.md)|  | [optional]

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="invoiceLinesPost"></a>
# **invoiceLinesPost**
> InlineResponse201 invoiceLinesPost(invoiceLine)

Add invoice

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.InvoiceLinesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    InvoiceLinesApi apiInstance = new InvoiceLinesApi(defaultClient);
    InlineObject7 invoiceLine = new InlineObject7(); // InlineObject7 | 
    try {
      InlineResponse201 result = apiInstance.invoiceLinesPost(invoiceLine);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling InvoiceLinesApi#invoiceLinesPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceLine** | [**InlineObject7**](InlineObject7.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

