

# InlineObject7

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  |  [optional]
**discountPercent** | **Integer** |  |  [optional]
**discountText** | **String** |  |  [optional]
**invoiceId** | [**UUID**](UUID.md) |  |  [optional]
**name** | **String** |  |  [optional]
**productId** | [**UUID**](UUID.md) |  |  [optional]
**quantity** | **Integer** |  |  [optional]
**sellingPrice** | **Float** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]



