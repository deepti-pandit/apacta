

# InlineResponse20044

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PaymentTerm**](PaymentTerm.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



