# MassMessagesUsersApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**massMessagesUsersGet**](MassMessagesUsersApi.md#massMessagesUsersGet) | **GET** /mass_messages_users | View list of mass messages for specific user
[**massMessagesUsersMassMessagesUserIdGet**](MassMessagesUsersApi.md#massMessagesUsersMassMessagesUserIdGet) | **GET** /mass_messages_users/{mass_messages_user_id} | View mass message
[**massMessagesUsersMassMessagesUserIdPut**](MassMessagesUsersApi.md#massMessagesUsersMassMessagesUserIdPut) | **PUT** /mass_messages_users/{mass_messages_user_id} | Edit mass message


<a name="massMessagesUsersGet"></a>
# **massMessagesUsersGet**
> InlineResponse20035 massMessagesUsersGet(isRead)

View list of mass messages for specific user

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MassMessagesUsersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MassMessagesUsersApi apiInstance = new MassMessagesUsersApi(defaultClient);
    Boolean isRead = true; // Boolean | Used to filter on the `is_read` of the mass messages
    try {
      InlineResponse20035 result = apiInstance.massMessagesUsersGet(isRead);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MassMessagesUsersApi#massMessagesUsersGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isRead** | **Boolean**| Used to filter on the &#x60;is_read&#x60; of the mass messages | [optional]

### Return type

[**InlineResponse20035**](InlineResponse20035.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="massMessagesUsersMassMessagesUserIdGet"></a>
# **massMessagesUsersMassMessagesUserIdGet**
> InlineResponse20036 massMessagesUsersMassMessagesUserIdGet(massMessagesUserId)

View mass message

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MassMessagesUsersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MassMessagesUsersApi apiInstance = new MassMessagesUsersApi(defaultClient);
    String massMessagesUserId = "massMessagesUserId_example"; // String | 
    try {
      InlineResponse20036 result = apiInstance.massMessagesUsersMassMessagesUserIdGet(massMessagesUserId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MassMessagesUsersApi#massMessagesUsersMassMessagesUserIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **massMessagesUserId** | **String**|  |

### Return type

[**InlineResponse20036**](InlineResponse20036.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="massMessagesUsersMassMessagesUserIdPut"></a>
# **massMessagesUsersMassMessagesUserIdPut**
> InlineResponse2004 massMessagesUsersMassMessagesUserIdPut(massMessagesUserId)

Edit mass message

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MassMessagesUsersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MassMessagesUsersApi apiInstance = new MassMessagesUsersApi(defaultClient);
    String massMessagesUserId = "massMessagesUserId_example"; // String | 
    try {
      InlineResponse2004 result = apiInstance.massMessagesUsersMassMessagesUserIdPut(massMessagesUserId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MassMessagesUsersApi#massMessagesUsersMassMessagesUserIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **massMessagesUserId** | **String**|  |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

