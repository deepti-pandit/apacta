

# InlineResponse20057

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;TimeEntry&gt;**](TimeEntry.md) |  |  [optional]
**pagination** | [**PaginationDetails**](PaginationDetails.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



