

# InlineResponse20028

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**FormTemplate**](FormTemplate.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



