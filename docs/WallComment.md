

# WallComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**message** | **String** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**userId** | **String** |  |  [optional]
**wallPostId** | [**UUID**](UUID.md) |  |  [optional]



