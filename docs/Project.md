

# Project

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactId** | [**UUID**](UUID.md) |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**erpProjectId** | **String** |  |  [optional]
**erpTaskId** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  | 
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  | 
**projectStatusId** | [**UUID**](UUID.md) |  |  [optional]
**startTime** | **String** |  |  [optional]
**streetName** | **String** |  |  [optional]



