

# InlineObject15

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactId** | [**UUID**](UUID.md) |  |  [optional]
**description** | **String** |  |  [optional]
**erpProjectId** | **String** |  |  [optional]
**erpTaskId** | **String** |  |  [optional]
**name** | **String** |  | 
**projectStatusId** | [**UUID**](UUID.md) |  |  [optional]
**startTime** | **String** |  |  [optional]
**streetName** | **String** |  |  [optional]



