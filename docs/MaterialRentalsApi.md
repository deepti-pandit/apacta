# MaterialRentalsApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**materialsMaterialIdRentalsCheckoutPost**](MaterialRentalsApi.md#materialsMaterialIdRentalsCheckoutPost) | **POST** /materials/{material_id}/rentals/checkout/ | Checkout material rental
[**materialsMaterialIdRentalsGet**](MaterialRentalsApi.md#materialsMaterialIdRentalsGet) | **GET** /materials/{material_id}/rentals/ | Show list of rentals for specific material
[**materialsMaterialIdRentalsMaterialRentalIdDelete**](MaterialRentalsApi.md#materialsMaterialIdRentalsMaterialRentalIdDelete) | **DELETE** /materials/{material_id}/rentals/{material_rental_id}/ | Delete material rental
[**materialsMaterialIdRentalsMaterialRentalIdGet**](MaterialRentalsApi.md#materialsMaterialIdRentalsMaterialRentalIdGet) | **GET** /materials/{material_id}/rentals/{material_rental_id}/ | Show rental foor materi
[**materialsMaterialIdRentalsMaterialRentalIdPut**](MaterialRentalsApi.md#materialsMaterialIdRentalsMaterialRentalIdPut) | **PUT** /materials/{material_id}/rentals/{material_rental_id}/ | Edit material rental
[**materialsMaterialIdRentalsPost**](MaterialRentalsApi.md#materialsMaterialIdRentalsPost) | **POST** /materials/{material_id}/rentals/ | Add material rental


<a name="materialsMaterialIdRentalsCheckoutPost"></a>
# **materialsMaterialIdRentalsCheckoutPost**
> InlineResponse201 materialsMaterialIdRentalsCheckoutPost(materialId, materialRental)

Checkout material rental

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialRentalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialRentalsApi apiInstance = new MaterialRentalsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    InlineObject12 materialRental = new InlineObject12(); // InlineObject12 | 
    try {
      InlineResponse201 result = apiInstance.materialsMaterialIdRentalsCheckoutPost(materialId, materialRental);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialRentalsApi#materialsMaterialIdRentalsCheckoutPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |
 **materialRental** | [**InlineObject12**](InlineObject12.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

<a name="materialsMaterialIdRentalsGet"></a>
# **materialsMaterialIdRentalsGet**
> InlineResponse20039 materialsMaterialIdRentalsGet(materialId)

Show list of rentals for specific material

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialRentalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialRentalsApi apiInstance = new MaterialRentalsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    try {
      InlineResponse20039 result = apiInstance.materialsMaterialIdRentalsGet(materialId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialRentalsApi#materialsMaterialIdRentalsGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |

### Return type

[**InlineResponse20039**](InlineResponse20039.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="materialsMaterialIdRentalsMaterialRentalIdDelete"></a>
# **materialsMaterialIdRentalsMaterialRentalIdDelete**
> InlineResponse20019 materialsMaterialIdRentalsMaterialRentalIdDelete(materialId, materialRentalId)

Delete material rental

Delete rental for material

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialRentalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialRentalsApi apiInstance = new MaterialRentalsApi(defaultClient);
    UUID materialId = new UUID(); // UUID | 
    UUID materialRentalId = new UUID(); // UUID | 
    try {
      InlineResponse20019 result = apiInstance.materialsMaterialIdRentalsMaterialRentalIdDelete(materialId, materialRentalId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialRentalsApi#materialsMaterialIdRentalsMaterialRentalIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | [**UUID**](.md)|  |
 **materialRentalId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="materialsMaterialIdRentalsMaterialRentalIdGet"></a>
# **materialsMaterialIdRentalsMaterialRentalIdGet**
> InlineResponse20040 materialsMaterialIdRentalsMaterialRentalIdGet(materialId, materialRentalId)

Show rental foor materi

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialRentalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialRentalsApi apiInstance = new MaterialRentalsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    String materialRentalId = "materialRentalId_example"; // String | 
    try {
      InlineResponse20040 result = apiInstance.materialsMaterialIdRentalsMaterialRentalIdGet(materialId, materialRentalId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialRentalsApi#materialsMaterialIdRentalsMaterialRentalIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |
 **materialRentalId** | **String**|  |

### Return type

[**InlineResponse20040**](InlineResponse20040.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**404** | Not found |  -  |

<a name="materialsMaterialIdRentalsMaterialRentalIdPut"></a>
# **materialsMaterialIdRentalsMaterialRentalIdPut**
> InlineResponse20019 materialsMaterialIdRentalsMaterialRentalIdPut(materialId, materialRentalId)

Edit material rental

Edit material rental

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialRentalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialRentalsApi apiInstance = new MaterialRentalsApi(defaultClient);
    UUID materialId = new UUID(); // UUID | 
    UUID materialRentalId = new UUID(); // UUID | 
    try {
      InlineResponse20019 result = apiInstance.materialsMaterialIdRentalsMaterialRentalIdPut(materialId, materialRentalId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialRentalsApi#materialsMaterialIdRentalsMaterialRentalIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | [**UUID**](.md)|  |
 **materialRentalId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="materialsMaterialIdRentalsPost"></a>
# **materialsMaterialIdRentalsPost**
> InlineResponse201 materialsMaterialIdRentalsPost(materialId, materialRental)

Add material rental

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialRentalsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialRentalsApi apiInstance = new MaterialRentalsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    InlineObject11 materialRental = new InlineObject11(); // InlineObject11 | 
    try {
      InlineResponse201 result = apiInstance.materialsMaterialIdRentalsPost(materialId, materialRental);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialRentalsApi#materialsMaterialIdRentalsPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |
 **materialRental** | [**InlineObject11**](InlineObject11.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

