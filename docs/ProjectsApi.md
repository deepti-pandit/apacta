# ProjectsApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**projectsGet**](ProjectsApi.md#projectsGet) | **GET** /projects | View list of projects
[**projectsPost**](ProjectsApi.md#projectsPost) | **POST** /projects | Add a project
[**projectsProjectIdDelete**](ProjectsApi.md#projectsProjectIdDelete) | **DELETE** /projects/{project_id} | Delete a project
[**projectsProjectIdFilesFileIdDelete**](ProjectsApi.md#projectsProjectIdFilesFileIdDelete) | **DELETE** /projects/{project_id}/files/{file_id}/ | Delete file
[**projectsProjectIdFilesFileIdGet**](ProjectsApi.md#projectsProjectIdFilesFileIdGet) | **GET** /projects/{project_id}/files/{file_id}/ | Show file
[**projectsProjectIdFilesFileIdPut**](ProjectsApi.md#projectsProjectIdFilesFileIdPut) | **PUT** /projects/{project_id}/files/{file_id}/ | Edit file
[**projectsProjectIdFilesGet**](ProjectsApi.md#projectsProjectIdFilesGet) | **GET** /projects/{project_id}/files | Show list of files uploaded to project
[**projectsProjectIdGet**](ProjectsApi.md#projectsProjectIdGet) | **GET** /projects/{project_id} | View specific project
[**projectsProjectIdProjectFilesGet**](ProjectsApi.md#projectsProjectIdProjectFilesGet) | **GET** /projects/{project_id}/project_files | Show list of project files uploaded to project
[**projectsProjectIdProjectFilesPost**](ProjectsApi.md#projectsProjectIdProjectFilesPost) | **POST** /projects/{project_id}/project_files | Add project file to projects
[**projectsProjectIdProjectFilesProjectFileIdDelete**](ProjectsApi.md#projectsProjectIdProjectFilesProjectFileIdDelete) | **DELETE** /projects/{project_id}/project_files/{project_file_id}/ | Delete project file
[**projectsProjectIdProjectFilesProjectFileIdGet**](ProjectsApi.md#projectsProjectIdProjectFilesProjectFileIdGet) | **GET** /projects/{project_id}/project_files/{project_file_id}/ | Show project file
[**projectsProjectIdProjectFilesProjectFileIdPut**](ProjectsApi.md#projectsProjectIdProjectFilesProjectFileIdPut) | **PUT** /projects/{project_id}/project_files/{project_file_id}/ | Edit project file
[**projectsProjectIdPut**](ProjectsApi.md#projectsProjectIdPut) | **PUT** /projects/{project_id} | Edit a project
[**projectsProjectIdUsersGet**](ProjectsApi.md#projectsProjectIdUsersGet) | **GET** /projects/{project_id}/users/ | Show list of users added to project
[**projectsProjectIdUsersPost**](ProjectsApi.md#projectsProjectIdUsersPost) | **POST** /projects/{project_id}/users/ | Add user to project
[**projectsProjectIdUsersUserIdDelete**](ProjectsApi.md#projectsProjectIdUsersUserIdDelete) | **DELETE** /projects/{project_id}/users/{user_id} | Delete user from project
[**projectsProjectIdUsersUserIdGet**](ProjectsApi.md#projectsProjectIdUsersUserIdGet) | **GET** /projects/{project_id}/users/{user_id} | View specific user assigned to project


<a name="projectsGet"></a>
# **projectsGet**
> InlineResponse20050 projectsGet(showAll, contactId, projectStatusId, projectStatusIds, name, erpProjectId, erpTaskId, startTimeGte, startTimeLte, startTimeEq)

View list of projects

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    Boolean showAll = false; // Boolean | Unless this is set to `true` only open projects will be shown
    UUID contactId = new UUID(); // UUID | Used to filter on the `contact_id` of the projects
    UUID projectStatusId = new UUID(); // UUID | Used to filter on the `project_status_id` of the projects
    List<UUID> projectStatusIds = Arrays.asList(); // List<UUID> | Used to filter on the `project_status_id` of the projects (match any of the provided values)
    String name = "name_example"; // String | Used to search on the `name` of the projects
    String erpProjectId = "erpProjectId_example"; // String | Used to search on the `erp_project_id` of the projects
    String erpTaskId = "erpTaskId_example"; // String | Used to search on the `erp_task_id` of the projects
    String startTimeGte = "startTimeGte_example"; // String | Show projects with start time after than this value
    String startTimeLte = "startTimeLte_example"; // String | Show projects with start time before this value
    String startTimeEq = "startTimeEq_example"; // String | Show only projects with start time on specific date
    try {
      InlineResponse20050 result = apiInstance.projectsGet(showAll, contactId, projectStatusId, projectStatusIds, name, erpProjectId, erpTaskId, startTimeGte, startTimeLte, startTimeEq);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **showAll** | **Boolean**| Unless this is set to &#x60;true&#x60; only open projects will be shown | [optional] [default to false]
 **contactId** | [**UUID**](.md)| Used to filter on the &#x60;contact_id&#x60; of the projects | [optional]
 **projectStatusId** | [**UUID**](.md)| Used to filter on the &#x60;project_status_id&#x60; of the projects | [optional]
 **projectStatusIds** | [**List&lt;UUID&gt;**](UUID.md)| Used to filter on the &#x60;project_status_id&#x60; of the projects (match any of the provided values) | [optional]
 **name** | **String**| Used to search on the &#x60;name&#x60; of the projects | [optional]
 **erpProjectId** | **String**| Used to search on the &#x60;erp_project_id&#x60; of the projects | [optional]
 **erpTaskId** | **String**| Used to search on the &#x60;erp_task_id&#x60; of the projects | [optional]
 **startTimeGte** | **String**| Show projects with start time after than this value | [optional]
 **startTimeLte** | **String**| Show projects with start time before this value | [optional]
 **startTimeEq** | **String**| Show only projects with start time on specific date | [optional]

### Return type

[**InlineResponse20050**](InlineResponse20050.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsPost"></a>
# **projectsPost**
> InlineResponse201 projectsPost(project)

Add a project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    InlineObject15 project = new InlineObject15(); // InlineObject15 | 
    try {
      InlineResponse201 result = apiInstance.projectsPost(project);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | [**InlineObject15**](InlineObject15.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

<a name="projectsProjectIdDelete"></a>
# **projectsProjectIdDelete**
> InlineResponse2005 projectsProjectIdDelete(projectId)

Delete a project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    try {
      InlineResponse2005 result = apiInstance.projectsProjectIdDelete(projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdFilesFileIdDelete"></a>
# **projectsProjectIdFilesFileIdDelete**
> InlineResponse20019 projectsProjectIdFilesFileIdDelete(projectId, fileId)

Delete file

Delete file uploaded to a project from wall post or forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    UUID projectId = new UUID(); // UUID | 
    UUID fileId = new UUID(); // UUID | 
    try {
      InlineResponse20019 result = apiInstance.projectsProjectIdFilesFileIdDelete(projectId, fileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdFilesFileIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | [**UUID**](.md)|  |
 **fileId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdFilesFileIdGet"></a>
# **projectsProjectIdFilesFileIdGet**
> InlineResponse20019 projectsProjectIdFilesFileIdGet(projectId, fileId)

Show file

Show file uploaded to a project from wall post or forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    String fileId = "fileId_example"; // String | 
    try {
      InlineResponse20019 result = apiInstance.projectsProjectIdFilesFileIdGet(projectId, fileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdFilesFileIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |
 **fileId** | **String**|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**404** | Not found |  -  |

<a name="projectsProjectIdFilesFileIdPut"></a>
# **projectsProjectIdFilesFileIdPut**
> InlineResponse20019 projectsProjectIdFilesFileIdPut(projectId, fileId)

Edit file

Edit file uploaded to a project from wall post or forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    UUID projectId = new UUID(); // UUID | 
    UUID fileId = new UUID(); // UUID | 
    try {
      InlineResponse20019 result = apiInstance.projectsProjectIdFilesFileIdPut(projectId, fileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdFilesFileIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | [**UUID**](.md)|  |
 **fileId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdFilesGet"></a>
# **projectsProjectIdFilesGet**
> InlineResponse20052 projectsProjectIdFilesGet(projectId)

Show list of files uploaded to project

Used to show files uploaded to a project from wall post or forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    try {
      InlineResponse20052 result = apiInstance.projectsProjectIdFilesGet(projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdFilesGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |

### Return type

[**InlineResponse20052**](InlineResponse20052.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdGet"></a>
# **projectsProjectIdGet**
> InlineResponse20051 projectsProjectIdGet(projectId)

View specific project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    try {
      InlineResponse20051 result = apiInstance.projectsProjectIdGet(projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |

### Return type

[**InlineResponse20051**](InlineResponse20051.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdProjectFilesGet"></a>
# **projectsProjectIdProjectFilesGet**
> InlineResponse20052 projectsProjectIdProjectFilesGet(projectId)

Show list of project files uploaded to project

Returns files belonging to the project, not uploaded from wall post or forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    try {
      InlineResponse20052 result = apiInstance.projectsProjectIdProjectFilesGet(projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdProjectFilesGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |

### Return type

[**InlineResponse20052**](InlineResponse20052.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdProjectFilesPost"></a>
# **projectsProjectIdProjectFilesPost**
> InlineResponse201 projectsProjectIdProjectFilesPost(projectId, file)

Add project file to projects

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    File file = new File("/path/to/file"); // File | 
    try {
      InlineResponse201 result = apiInstance.projectsProjectIdProjectFilesPost(projectId, file);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdProjectFilesPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |
 **file** | **File**|  |

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: multipart/forms-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successfully added project file |  -  |
**422** | Validation error |  -  |

<a name="projectsProjectIdProjectFilesProjectFileIdDelete"></a>
# **projectsProjectIdProjectFilesProjectFileIdDelete**
> InlineResponse20019 projectsProjectIdProjectFilesProjectFileIdDelete(projectFileId, projectId)

Delete project file

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    UUID projectFileId = new UUID(); // UUID | 
    UUID projectId = new UUID(); // UUID | 
    try {
      InlineResponse20019 result = apiInstance.projectsProjectIdProjectFilesProjectFileIdDelete(projectFileId, projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdProjectFilesProjectFileIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectFileId** | [**UUID**](.md)|  |
 **projectId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdProjectFilesProjectFileIdGet"></a>
# **projectsProjectIdProjectFilesProjectFileIdGet**
> InlineResponse20019 projectsProjectIdProjectFilesProjectFileIdGet(projectId, projectFileId)

Show project file

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    String projectFileId = "projectFileId_example"; // String | 
    try {
      InlineResponse20019 result = apiInstance.projectsProjectIdProjectFilesProjectFileIdGet(projectId, projectFileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdProjectFilesProjectFileIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |
 **projectFileId** | **String**|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**404** | Not found |  -  |

<a name="projectsProjectIdProjectFilesProjectFileIdPut"></a>
# **projectsProjectIdProjectFilesProjectFileIdPut**
> InlineResponse20019 projectsProjectIdProjectFilesProjectFileIdPut(projectId, projectFileId)

Edit project file

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    UUID projectId = new UUID(); // UUID | 
    UUID projectFileId = new UUID(); // UUID | 
    try {
      InlineResponse20019 result = apiInstance.projectsProjectIdProjectFilesProjectFileIdPut(projectId, projectFileId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdProjectFilesProjectFileIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | [**UUID**](.md)|  |
 **projectFileId** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20019**](InlineResponse20019.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdPut"></a>
# **projectsProjectIdPut**
> InlineResponse2004 projectsProjectIdPut(projectId, project)

Edit a project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    InlineObject16 project = new InlineObject16(); // InlineObject16 | 
    try {
      InlineResponse2004 result = apiInstance.projectsProjectIdPut(projectId, project);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |
 **project** | [**InlineObject16**](InlineObject16.md)|  | [optional]

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdUsersGet"></a>
# **projectsProjectIdUsersGet**
> InlineResponse20053 projectsProjectIdUsersGet(projectId)

Show list of users added to project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    try {
      InlineResponse20053 result = apiInstance.projectsProjectIdUsersGet(projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdUsersGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |

### Return type

[**InlineResponse20053**](InlineResponse20053.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdUsersPost"></a>
# **projectsProjectIdUsersPost**
> InlineResponse201 projectsProjectIdUsersPost(projectId, userId)

Add user to project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String projectId = "projectId_example"; // String | 
    InlineObject17 userId = new InlineObject17(); // InlineObject17 | 
    try {
      InlineResponse201 result = apiInstance.projectsProjectIdUsersPost(projectId, userId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdUsersPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **String**|  |
 **userId** | [**InlineObject17**](InlineObject17.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

<a name="projectsProjectIdUsersUserIdDelete"></a>
# **projectsProjectIdUsersUserIdDelete**
> InlineResponse2004 projectsProjectIdUsersUserIdDelete(userId, projectId)

Delete user from project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String userId = "userId_example"; // String | 
    String projectId = "projectId_example"; // String | 
    try {
      InlineResponse2004 result = apiInstance.projectsProjectIdUsersUserIdDelete(userId, projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdUsersUserIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**|  |
 **projectId** | **String**|  |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="projectsProjectIdUsersUserIdGet"></a>
# **projectsProjectIdUsersUserIdGet**
> InlineResponse20054 projectsProjectIdUsersUserIdGet(userId, projectId)

View specific user assigned to project

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.ProjectsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    ProjectsApi apiInstance = new ProjectsApi(defaultClient);
    String userId = "userId_example"; // String | 
    String projectId = "projectId_example"; // String | 
    try {
      InlineResponse20054 result = apiInstance.projectsProjectIdUsersUserIdGet(userId, projectId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProjectsApi#projectsProjectIdUsersUserIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**|  |
 **projectId** | **String**|  |

### Return type

[**InlineResponse20054**](InlineResponse20054.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

