

# InlineResponse20060

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**TimeEntryInterval**](TimeEntryInterval.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



