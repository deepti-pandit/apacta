

# InlineResponse20056

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**StockLocation**](StockLocation.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



