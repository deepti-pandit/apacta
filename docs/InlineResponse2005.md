

# InlineResponse2005

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **List&lt;String&gt;** |  |  [optional]
**success** | **Boolean** |  |  [optional]



