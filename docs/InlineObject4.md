

# InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactId** | [**UUID**](UUID.md) |  |  [optional]
**currencyId** | [**UUID**](UUID.md) |  |  [optional]
**deliveryDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**description** | **String** |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**reference** | **String** |  |  [optional]
**shortText** | **String** |  |  [optional]
**supplierInvoiceNumber** | **String** |  |  [optional]



