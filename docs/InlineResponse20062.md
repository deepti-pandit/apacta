

# InlineResponse20062

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**TimeEntryType**](TimeEntryType.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



