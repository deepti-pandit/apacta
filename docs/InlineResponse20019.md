

# InlineResponse20019

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Object**](.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



