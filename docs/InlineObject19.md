

# InlineObject19

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**formId** | [**UUID**](UUID.md) |  |  [optional]
**fromTime** | **String** |  |  [optional]
**isAllDay** | **Boolean** |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**sum** | **Integer** | Amount of seconds - should only be included when using is_all_day, otherwise will be calculated from from_time and to_time |  [optional]
**timeEntryTypeId** | [**UUID**](UUID.md) |  | 
**toTime** | **String** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  | 



