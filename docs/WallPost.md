

# WallPost

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**message** | **String** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]



