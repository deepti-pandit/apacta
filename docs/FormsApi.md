# FormsApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**formsFormIdDelete**](FormsApi.md#formsFormIdDelete) | **DELETE** /forms/{form_id} | Delete a forms
[**formsFormIdGet**](FormsApi.md#formsFormIdGet) | **GET** /forms/{form_id} | View forms
[**formsFormIdPut**](FormsApi.md#formsFormIdPut) | **PUT** /forms/{form_id} | Edit a forms
[**formsGet**](FormsApi.md#formsGet) | **GET** /forms | Retrieve array of forms
[**formsPost**](FormsApi.md#formsPost) | **POST** /forms | Add new forms


<a name="formsFormIdDelete"></a>
# **formsFormIdDelete**
> formsFormIdDelete(formId)

Delete a forms

You can only delete the forms that you&#39;ve submitted yourself

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.FormsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    FormsApi apiInstance = new FormsApi(defaultClient);
    String formId = "formId_example"; // String | 
    try {
      apiInstance.formsFormIdDelete(formId);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormsApi#formsFormIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **formId** | **String**|  |

### Return type

null (empty response body)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="formsFormIdGet"></a>
# **formsFormIdGet**
> InlineResponse20030 formsFormIdGet(formId)

View forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.FormsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    FormsApi apiInstance = new FormsApi(defaultClient);
    String formId = "formId_example"; // String | 
    try {
      InlineResponse20030 result = apiInstance.formsFormIdGet(formId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormsApi#formsFormIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **formId** | **String**|  |

### Return type

[**InlineResponse20030**](InlineResponse20030.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="formsFormIdPut"></a>
# **formsFormIdPut**
> formsFormIdPut(formId)

Edit a forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.FormsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    FormsApi apiInstance = new FormsApi(defaultClient);
    String formId = "formId_example"; // String | 
    try {
      apiInstance.formsFormIdPut(formId);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormsApi#formsFormIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **formId** | **String**|  |

### Return type

null (empty response body)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="formsGet"></a>
# **formsGet**
> InlineResponse20029 formsGet(extended, dateFrom, dateTo, projectId, createdById, formTemplateId)

Retrieve array of forms

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.FormsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    FormsApi apiInstance = new FormsApi(defaultClient);
    String extended = "extended_example"; // String | Used to have extended details from the forms from the `Form`'s `FormFields`
    String dateFrom = "dateFrom_example"; // String | Used in conjunction with `date_to` to only show forms within these dates - format like `2016-28-05`
    String dateTo = "dateTo_example"; // String | Used in conjunction with `date_from` to only show forms within these dates - format like `2016-28-30`
    UUID projectId = new UUID(); // UUID | Used to filter on the `project_id` of the forms
    String createdById = "createdById_example"; // String | Used to filter on the `created_by_id` of the forms
    String formTemplateId = "formTemplateId_example"; // String | Used to filter on the `form_template_id` of the forms
    try {
      InlineResponse20029 result = apiInstance.formsGet(extended, dateFrom, dateTo, projectId, createdById, formTemplateId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormsApi#formsGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **extended** | **String**| Used to have extended details from the forms from the &#x60;Form&#x60;&#39;s &#x60;FormFields&#x60; | [optional] [enum: true, false]
 **dateFrom** | **String**| Used in conjunction with &#x60;date_to&#x60; to only show forms within these dates - format like &#x60;2016-28-05&#x60; | [optional]
 **dateTo** | **String**| Used in conjunction with &#x60;date_from&#x60; to only show forms within these dates - format like &#x60;2016-28-30&#x60; | [optional]
 **projectId** | [**UUID**](.md)| Used to filter on the &#x60;project_id&#x60; of the forms | [optional]
 **createdById** | **String**| Used to filter on the &#x60;created_by_id&#x60; of the forms | [optional]
 **formTemplateId** | **String**| Used to filter on the &#x60;form_template_id&#x60; of the forms | [optional]

### Return type

[**InlineResponse20029**](InlineResponse20029.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="formsPost"></a>
# **formsPost**
> InlineResponse201 formsPost(forms)

Add new forms

Used to add a forms into the system

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.FormsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    FormsApi apiInstance = new FormsApi(defaultClient);
    InlineObject6 forms = new InlineObject6(); // InlineObject6 | 
    try {
      InlineResponse201 result = apiInstance.formsPost(forms);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormsApi#formsPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forms** | [**InlineObject6**](InlineObject6.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

