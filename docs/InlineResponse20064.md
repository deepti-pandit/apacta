

# InlineResponse20064

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**TimeEntryUnitType**](TimeEntryUnitType.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



