

# InlineResponse20018

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **List&lt;String&gt;** |  |  [optional]
**success** | **Boolean** |  |  [optional]



