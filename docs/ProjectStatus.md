

# ProjectStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**identifier** | [**IdentifierEnum**](#IdentifierEnum) | One of 3 values |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]



## Enum: IdentifierEnum

Name | Value
---- | -----
READY_FOR_BILLING | &quot;ready_for_billing&quot;
OPEN | &quot;open&quot;
CLOSED | &quot;closed&quot;



