

# User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | [**UUID**](UUID.md) |  |  [optional]
**cityId** | [**UUID**](UUID.md) |  |  [optional]
**companyId** | [**UUID**](UUID.md) |  |  [optional]
**costPrice** | **Float** | Cost of salaries |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**email** | **String** |  |  [optional]
**extraPrice** | **Float** | Additional cost on this employee (pension, vacation etc.) |  [optional]
**firstName** | **String** |  |  [optional]
**fullName** | **String** | READ-ONLY |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**isActive** | **Boolean** |  |  [optional]
**languageId** | [**UUID**](UUID.md) |  |  [optional]
**lastName** | **String** |  |  [optional]
**mobile** | **String** |  |  [optional]
**mobileCountrycode** | **String** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**password** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**phoneCountrycode** | **String** |  |  [optional]
**receiveFormMails** | **Boolean** | If &#x60;true&#x60; the employee will receive an email receipt of every forms submitted |  [optional]
**salePrice** | **Float** | The price this employee costs per hour when working |  [optional]
**streetName** | **String** |  |  [optional]
**website** | **String** |  |  [optional]



