

# InvoiceLine

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**description** | **String** |  |  [optional]
**discountPercent** | **Integer** |  |  [optional]
**discountText** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**invoiceId** | [**UUID**](UUID.md) |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**name** | **String** |  |  [optional]
**productId** | [**UUID**](UUID.md) |  |  [optional]
**quantity** | **Integer** |  |  [optional]
**sellingPrice** | **Float** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]



