# MaterialsApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**materialsGet**](MaterialsApi.md#materialsGet) | **GET** /materials | View list of all materials
[**materialsMaterialIdDelete**](MaterialsApi.md#materialsMaterialIdDelete) | **DELETE** /materials/{material_id} | Delete material
[**materialsMaterialIdGet**](MaterialsApi.md#materialsMaterialIdGet) | **GET** /materials/{material_id} | View material
[**materialsMaterialIdPut**](MaterialsApi.md#materialsMaterialIdPut) | **PUT** /materials/{material_id} | Edit material
[**materialsMaterialIdRentalsMaterialRentalIdPost**](MaterialsApi.md#materialsMaterialIdRentalsMaterialRentalIdPost) | **POST** /materials/{material_id}/rentals/{material_rental_id}/ | Add material


<a name="materialsGet"></a>
# **materialsGet**
> InlineResponse20037 materialsGet(barcode, name, projectId, currentlyRented)

View list of all materials

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialsApi apiInstance = new MaterialsApi(defaultClient);
    String barcode = "barcode_example"; // String | Used to filter on the `barcode` of the materials
    String name = "name_example"; // String | Used to filter on the `name` of the materials
    UUID projectId = new UUID(); // UUID | Used to find materials used in specific project by `project_id`
    Boolean currentlyRented = true; // Boolean | Used to find currently rented materials
    try {
      InlineResponse20037 result = apiInstance.materialsGet(barcode, name, projectId, currentlyRented);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialsApi#materialsGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **barcode** | **String**| Used to filter on the &#x60;barcode&#x60; of the materials | [optional]
 **name** | **String**| Used to filter on the &#x60;name&#x60; of the materials | [optional]
 **projectId** | [**UUID**](.md)| Used to find materials used in specific project by &#x60;project_id&#x60; | [optional]
 **currentlyRented** | **Boolean**| Used to find currently rented materials | [optional]

### Return type

[**InlineResponse20037**](InlineResponse20037.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="materialsMaterialIdDelete"></a>
# **materialsMaterialIdDelete**
> InlineResponse2004 materialsMaterialIdDelete(materialId)

Delete material

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialsApi apiInstance = new MaterialsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    try {
      InlineResponse2004 result = apiInstance.materialsMaterialIdDelete(materialId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialsApi#materialsMaterialIdDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="materialsMaterialIdGet"></a>
# **materialsMaterialIdGet**
> InlineResponse20038 materialsMaterialIdGet(materialId)

View material

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialsApi apiInstance = new MaterialsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    try {
      InlineResponse20038 result = apiInstance.materialsMaterialIdGet(materialId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialsApi#materialsMaterialIdGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |

### Return type

[**InlineResponse20038**](InlineResponse20038.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="materialsMaterialIdPut"></a>
# **materialsMaterialIdPut**
> InlineResponse2004 materialsMaterialIdPut(materialId)

Edit material

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialsApi apiInstance = new MaterialsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    try {
      InlineResponse2004 result = apiInstance.materialsMaterialIdPut(materialId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialsApi#materialsMaterialIdPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

<a name="materialsMaterialIdRentalsMaterialRentalIdPost"></a>
# **materialsMaterialIdRentalsMaterialRentalIdPost**
> InlineResponse201 materialsMaterialIdRentalsMaterialRentalIdPost(materialId, materialRentalId, material)

Add material

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.MaterialsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    MaterialsApi apiInstance = new MaterialsApi(defaultClient);
    String materialId = "materialId_example"; // String | 
    String materialRentalId = "materialRentalId_example"; // String | 
    InlineObject13 material = new InlineObject13(); // InlineObject13 | 
    try {
      InlineResponse201 result = apiInstance.materialsMaterialIdRentalsMaterialRentalIdPost(materialId, materialRentalId, material);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MaterialsApi#materialsMaterialIdRentalsMaterialRentalIdPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materialId** | **String**|  |
 **materialRentalId** | **String**|  |
 **material** | [**InlineObject13**](InlineObject13.md)|  | [optional]

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | OK |  -  |
**422** | Validation error |  -  |

