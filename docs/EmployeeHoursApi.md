# EmployeeHoursApi

All URIs are relative to *https://app.apacta.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**employeeHoursGet**](EmployeeHoursApi.md#employeeHoursGet) | **GET** /employee_hours | Used to retrieve details about the logged in user&#39;s hours


<a name="employeeHoursGet"></a>
# **employeeHoursGet**
> InlineResponse20016 employeeHoursGet(dateFrom, dateTo)

Used to retrieve details about the logged in user&#39;s hours

### Example

```java
// Import classes:
import com.apacta.ApiClient;
import com.apacta.ApiException;
import com.apacta.Configuration;
import com.apacta.auth.*;
import com.apacta.models.*;
import org.openapitools.client.api.EmployeeHoursApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://app.apacta.com/api/v1");
    
    // Configure API key authorization: X-Auth-Token
    ApiKeyAuth X-Auth-Token = (ApiKeyAuth) defaultClient.getAuthentication("X-Auth-Token");
    X-Auth-Token.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //X-Auth-Token.setApiKeyPrefix("Token");

    // Configure API key authorization: api_key
    ApiKeyAuth api_key = (ApiKeyAuth) defaultClient.getAuthentication("api_key");
    api_key.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //api_key.setApiKeyPrefix("Token");

    EmployeeHoursApi apiInstance = new EmployeeHoursApi(defaultClient);
    String dateFrom = "dateFrom_example"; // String | Date formatted as Y-m-d (2016-06-28)
    String dateTo = "dateTo_example"; // String | Date formatted as Y-m-d (2016-06-28)
    try {
      InlineResponse20016 result = apiInstance.employeeHoursGet(dateFrom, dateTo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EmployeeHoursApi#employeeHoursGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dateFrom** | **String**| Date formatted as Y-m-d (2016-06-28) |
 **dateTo** | **String**| Date formatted as Y-m-d (2016-06-28) |

### Return type

[**InlineResponse20016**](InlineResponse20016.md)

### Authorization

[X-Auth-Token](../README.md#X-Auth-Token), [api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

