

# InlineObject9

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactId** | [**UUID**](UUID.md) |  |  [optional]
**createdOrModifiedGte** | [**LocalDate**](LocalDate.md) |  |  [optional]
**dateFrom** | [**LocalDate**](LocalDate.md) |  |  [optional]
**dateTo** | [**LocalDate**](LocalDate.md) |  |  [optional]
**erpId** | **String** |  |  [optional]
**erpPaymentTermId** | **String** |  |  [optional]
**invoiceNumber** | **Integer** |  |  [optional]
**isDraft** | **Boolean** |  |  [optional]
**isLocked** | **Boolean** |  |  [optional]
**isOffer** | **Boolean** |  |  [optional]
**issuedDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**message** | **String** |  |  [optional]
**offerNumber** | **Integer** |  |  [optional]
**paymentDueDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**paymentTermId** | [**UUID**](UUID.md) |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**reference** | **String** |  |  [optional]
**vatPercent** | **Integer** |  |  [optional]



