

# InlineResponse20068

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**VendorProduct**](VendorProduct.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



