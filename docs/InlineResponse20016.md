

# InlineResponse20016

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;InlineResponse20016Data&gt;**](InlineResponse20016Data.md) | One element per forms in the period |  [optional]
**success** | **Boolean** |  |  [optional]



