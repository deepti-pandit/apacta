

# InlineResponse20069

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**WallComment**](WallComment.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



