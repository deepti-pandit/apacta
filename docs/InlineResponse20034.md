

# InlineResponse20034

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Invoice**](Invoice.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



