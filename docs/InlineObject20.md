

# InlineObject20

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  |  [optional]
**name** | **String** |  | 
**timeEntryIntervalId** | [**UUID**](UUID.md) |  | 
**timeEntryValueTypeId** | [**UUID**](UUID.md) |  | 



