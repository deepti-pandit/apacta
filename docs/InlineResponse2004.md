

# InlineResponse2004

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **List&lt;Object&gt;** |  |  [optional]
**success** | **Boolean** |  |  [optional]



