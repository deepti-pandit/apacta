

# InlineResponse2001

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**City**](City.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



