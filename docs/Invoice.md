

# Invoice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyId** | [**UUID**](UUID.md) |  |  [optional]
**contactId** | [**UUID**](UUID.md) |  |  [optional]
**created** | **String** | READ-ONLY |  [optional]
**createdById** | [**UUID**](UUID.md) |  |  [optional]
**currencyId** | [**UUID**](UUID.md) |  |  [optional]
**dateFrom** | [**LocalDate**](LocalDate.md) |  |  [optional]
**dateTo** | [**LocalDate**](LocalDate.md) |  |  [optional]
**deleted** | **String** | READ-ONLY - only present if it&#39;s an deleted object |  [optional]
**erpId** | **String** |  |  [optional]
**erpPaymentTermId** | **String** |  |  [optional]
**euCustomer** | **Boolean** |  |  [optional]
**grossPayment** | **Float** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**integrationId** | [**UUID**](UUID.md) |  |  [optional]
**invoiceNumber** | **Integer** |  |  [optional]
**isDraft** | **Boolean** |  |  [optional]
**isLocked** | **Boolean** |  |  [optional]
**isOffer** | **Boolean** |  |  [optional]
**issuedDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**message** | **String** |  |  [optional]
**modified** | **String** | READ-ONLY |  [optional]
**netPayment** | **Float** |  |  [optional]
**offerNumber** | **Integer** |  |  [optional]
**paymentDueDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**paymentTermId** | [**UUID**](UUID.md) |  |  [optional]
**projectId** | [**UUID**](UUID.md) |  |  [optional]
**reference** | **String** |  |  [optional]
**vatPercent** | **Integer** |  |  [optional]



