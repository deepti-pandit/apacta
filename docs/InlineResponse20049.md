

# InlineResponse20049

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**ProjectStatus**](ProjectStatus.md) |  |  [optional]
**success** | **Boolean** |  |  [optional]



